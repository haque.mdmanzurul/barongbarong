jQuery(document).ready(function($) {

	// Close export metabox by default
	$("#cpt_code_metabox.postbox").addClass("closed");

	/*
	|--------------------------------------------------------------------------
	| CHECK EMPTY REQUIRED FIELDS
	|--------------------------------------------------------------------------
	*/
	// CPT name
	$('#rc_wctg_metabox_name').blur(function()
	{
	    if(!$.trim(this.value).length) {
	          $(this).addClass('required-field');
	    } else {
		    $(this).removeClass('required-field');
	    }

	});
	// Singular label
	$('input.rc_wctg_metabox_ids_fields').blur(function()
	{
	    if(!$.trim(this.value).length) {
	          $(this).addClass('required-field');
	    } else {
		    $(this).removeClass('required-field');
	    }

	});


	/*
	|--------------------------------------------------------------------------
	| Add new field row
	|--------------------------------------------------------------------------
	*/


  	/**--------------------------
	 * Initialize Repeatable fields
	 */
	$( ".rc_wctg_repeatable_fields" ).sortable({ 
		cursor: "move",
		update: function() {
			 $(this).find('p').each( function(i) {
				$(this).find('input:text, select').each( function(f) {
					var name = $(this).attr('name').replace(/\[[0-9]*\]\[[0-9]*\]/, "");
					$(this).attr('name', name+"["+i+"]["+f+"]");
				});
			 });
		}
	});
	
	$('.rc_wctg_add_repeatable_field').click( function() {
		// var clone = $('#cmb_repeatable_fields p:first-child').clone();
		var clone = $(this).parent().find('.rc_wctg_repeatable_fields p:first-child').clone();
		var i = $(this).parent().find('.rc_wctg_repeatable_fields p').size();
		clone.find("input:text, select").val("");
		
		clone.find("input:text, select").each( function(f) {
			var name = $(this).attr('name').replace(/\[[0-9]*\]\[[0-9]*\]/, "");
			$(this).attr('name', name+"["+i+"]["+f+"]");
		});
		
		clone.appendTo($(this).parent().find('.rc_wctg_repeatable_fields'));
	});
	
	$('.rc_wctg_remove_repeatable_field').live('click', function() {
		if ( $(this).parents('.rc_wctg_repeatable_fields').find('p').size() > 1 )
		{
			$(this).parent().remove();
		}
	});


});

