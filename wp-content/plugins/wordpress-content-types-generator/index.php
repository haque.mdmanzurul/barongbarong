<?php
/*
Plugin Name: WordPress Content Types Generator
Plugin URL: http://remicorson.com/wordpress-content-types-generator
Description: An ideal solution to easily create custom post types and taxonomies
Version: 1.1
Author: Remi Corson
Author URI: http://remicorson.com
Contributors: corsonr
Text Domain: rc_wctg
Domain Path: languages
*/


/*
|--------------------------------------------------------------------------
| ERRORS DISPLAY
|--------------------------------------------------------------------------
*/

//@ini_set('display_errors', 'on');

/*
|--------------------------------------------------------------------------
| CONSTANTS
|--------------------------------------------------------------------------
*/

if( !defined( 'RC_WCTG_BASE_FILE' ) )		define( 'RC_WCTG_BASE_FILE', __FILE__ );
if( !defined( 'RC_WCTG_BASE_DIR' ) ) 		define( 'RC_WCTG_BASE_DIR', dirname( RC_WCTG_BASE_FILE ) );
if( !defined( 'RC_WCTG_PLUGIN_URL' ) ) 		define( 'RC_WCTG_PLUGIN_URL', plugin_dir_url( __FILE__ ) );
if( !defined( 'RC_WCTG_PLUGIN_VERSION' ) ) 	define( 'RC_WCTG_PLUGIN_VERSION', '1.1' );


/*
|--------------------------------------------------------------------------
| GLOBALS
|--------------------------------------------------------------------------
*/

global $rc_wctg_options;

/*
|--------------------------------------------------------------------------
| INTERNATIONALIZATION
|--------------------------------------------------------------------------
*/

function rc_wctg_textdomain() {
	load_plugin_textdomain( 'rc_wctg', false, dirname( plugin_basename( RC_WCTG_BASE_FILE ) ) . '/languages/' );
}
add_action('init', 'rc_wctg_textdomain');


/*
|--------------------------------------------------------------------------
| INCLUDES
|--------------------------------------------------------------------------
*/

// Plugin Options
include_once( RC_WCTG_BASE_DIR . '/includes/settings/register-settings.php' );
$rc_wctg_options = rc_wctg_get_settings();

// Functions
include_once( RC_WCTG_BASE_DIR . '/includes/functions.php' );
include_once( RC_WCTG_BASE_DIR . '/includes/pluggable-functions.php' );

// Shortcodes
include_once( RC_WCTG_BASE_DIR . '/includes/shortcodes.php' );

// Scripts & styles
include_once( RC_WCTG_BASE_DIR . '/includes/scripts.php' );

// Post Types
include_once( RC_WCTG_BASE_DIR . '/includes/post-types/post-types.php');
include_once( RC_WCTG_BASE_DIR . '/includes/post-types/post-types-columns.php');
include_once( RC_WCTG_BASE_DIR . '/includes/post-types/post-types-functions.php');

// Taxonomies
include_once( RC_WCTG_BASE_DIR . '/includes/taxonomies/taxonomies-columns.php');
include_once( RC_WCTG_BASE_DIR . '/includes/taxonomies/taxonomies-functions.php');

// Metaboxes
include_once( RC_WCTG_BASE_DIR . '/includes/metaboxes/metaboxes-columns.php');
include_once( RC_WCTG_BASE_DIR . '/includes/metaboxes/metaboxes-functions.php');

// Install, Un-install, Deactivate
include_once( RC_WCTG_BASE_DIR . '/includes/install/install.php' );
include_once( RC_WCTG_BASE_DIR . '/includes/install/uninstall.php' );
include_once( RC_WCTG_BASE_DIR . '/includes/install/deactivate.php' );

// Admin only
if( is_admin() ) {
	require_once RC_WCTG_BASE_DIR . '/includes/admin-pages.php';
	require_once RC_WCTG_BASE_DIR . '/includes/admin-pages/settings.php';
	
	// Contextual Help
	include_once( RC_WCTG_BASE_DIR . '/includes/admin/contextual-help.php' );
	
	// Metaboxes
	require_once RC_WCTG_BASE_DIR . '/includes/admin/post-types-metaboxes.php';
	require_once RC_WCTG_BASE_DIR . '/includes/admin/taxonomies-metaboxes.php';
	require_once RC_WCTG_BASE_DIR . '/includes/admin/metaboxes-metaboxes.php';
}