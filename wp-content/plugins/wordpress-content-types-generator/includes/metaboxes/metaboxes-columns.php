<?php

/*
|--------------------------------------------------------------------------
| metaboxes Columns
|--------------------------------------------------------------------------
*/

/**
 * Load custom metabox columns
 *
 * @access      public
 * @since       1.0 
 * @return      
*/
function rc_wctg_metabox_columns($columns)
{

	$newcolumns = array(
		"cb"       	  => "<input type  = \"checkbox\" />",
		"title"       => "",
		"cpt"      => esc_html__('CPTs', 'rc_wctg'),
		"fields"      => esc_html__('Fields', 'rc_wctg'),
		"description" => esc_html__('Description', 'rc_wctg'),
	);

	
	$columns = array_merge($newcolumns, $columns);
	
	return $columns;
}


/**
 * Charge metabox columns content
 *
 * @access      public
 * @since       1.0 
 * @return      
*/
function rc_wctg_metabox_columns_content($column)
{
	global $post;
	
	$metabox_id = $post->ID;
	
	// metabox data
	$rc_wctg_metabox_name   = get_post_meta($post->ID, 'rc_wctg_metabox_name', true);
    $rc_wctg_link_to      	= get_post_meta($post->ID, 'rc_wctg_link_to', true);
    $rc_wctg_metabox_fields	= get_post_meta($post->ID, 'rc_wctg_metabox_fields', true);
    $rc_wctg_description    = get_post_meta($post->ID, 'rc_wctg_description', true);

	switch ($column)
	{
		case "cpt":
	        $count_link_to = count($rc_wctg_link_to);
	        $i = 1;
		    foreach( $rc_wctg_link_to as $rc_wctg_link ) {
		        $link_to .= "$rc_wctg_link";
		        if( $i < $count_link_to)
		        	$link_to .= ', ';
		        $i++;
		    }
		    echo $link_to;
		break;
		case "fields":
	        $count_fields = count($rc_wctg_metabox_fields);
	        $j = 1;

		    foreach( $rc_wctg_metabox_fields as $field ) {
		        $fields .= $field[0];
		        if( $j < $count_fields)
		        	$fields .= ', ';
		        $j++;
		    }
		    echo $fields;
		break;	
		case "description":
			echo $rc_wctg_description;
		break;	
		
	}
}


/**
 * Load metaboxes columns
 *
 * @access      public
 * @since       1.0 
 * @return      
*/
function rc_wctg_load_metaboxes_columns() {
	
	global $typenow;
	
	if( $typenow == 'metabox' ) {
		add_filter("manage_edit-metabox_columns", "rc_wctg_metabox_columns");
		add_action("manage_posts_custom_column",  "rc_wctg_metabox_columns_content");
	}
}

add_action("admin_init",  "rc_wctg_load_metaboxes_columns");