<?php
/**
 * Register Settings
 *
*/

// Exit if accessed directly
if ( !defined( 'ABSPATH' ) ) exit;

/**
 * Register Settings
 *
 * Registers the required settings.
 *
 * @access      private
 * @since       1.0
 * @return      void
*/

function rc_wctg_register_settings() {

	// Setup some default option sets
	$pages = get_pages();
	$pages_options = array( 0 => '' ); // Blank option
	if( $pages ) {
		foreach( $pages as $page ) {
			$pages_options[ $page->ID ] = $page->post_title;
		}
	}

	/* white list our settings, each in their respective section
	   filters can be used to add more options to each section */
	$rc_wctg_settings = array(
		// General Settings
		'general' => apply_filters('rc_wctg_settings_general',
			array(
				array(
					'id' => 'minimum_level',
					'name' => __('Minimum level', 'rc_wctg'),
					'desc' => __('Choose minimum user level required to use the plugin.', 'rc_wctg'),
					'type' => 'select',
					'options' => array(
						'administrator' => __('Administrator', 'rc_wctg'),
						'editor'        => __('Editor', 'rc_wctg'),
						'author'        => __('Author', 'rc_wctg'),
						'contributor'   => __('Contributor', 'rc_wctg'),
						'subscriber'    => __('Subscriber', 'rc_wctg'),
					)
				)
			)
		), // End general
	
	);

	/*
	|--------------------------------------------------------------------------
	| Section General
	|--------------------------------------------------------------------------
	*/
	if( false == get_option( 'rc_wctg_settings_general' ) ) {
		add_option( 'rc_wctg_settings_general' );
	}

	add_settings_section(
		'rc_wctg_settings_general',
		__('General Settings', 'rc_wctg'),
		'__return_false',
		'rc_wctg_settings_general'
	);

	foreach( $rc_wctg_settings['general'] as $option ) {
		add_settings_field(
			'rc_wctg_settings_general[' . $option['id'] . ']',
			$option['name'],
			function_exists( 'rc_wctg_' . $option['type'] . '_callback' ) ? 'rc_wctg_' . $option['type'] . '_callback' : 'rc_wctg_missing_callback',
			'rc_wctg_settings_general',
			'rc_wctg_settings_general',
			array(
				'id' => $option['id'],
				'desc' => $option['desc'],
				'name' => $option['name'],
				'section' => 'general',
				'size' => isset($option['size']) ? $option['size'] : null,
				'options' => isset($option['options']) ? $option['options'] : '',
				'std' => isset($option['std']) ? $option['std'] : ''
			)
		);
	}


	// Creates our settings in the options table
	register_setting( 'rc_wctg_settings_general', 'rc_wctg_settings_general', 'rc_wctg_settings_sanitize' );


}
add_action('admin_init', 'rc_wctg_register_settings');


/**
 * Header Callback
 *
 * Renders the header.
 *
 * @access      private
 * @since       1.0
 * @return      void
*/

function rc_wctg_header_callback($args) {
	echo '';
}


/**
 * Checkbox Callback
 *
 * Renders checkboxes.
 *
 * @access      private
 * @since       1.0
 * @return      void
*/

function rc_wctg_checkbox_callback($args) {
	global $rc_wctg_options;

	$checked = isset($rc_wctg_options[$args['id']]) ? checked(1, $rc_wctg_options[$args['id']], false) : '';
	$html = '<input type="checkbox" id="rc_wctg_settings_' . $args['section'] . '[' . $args['id'] . ']" name="rc_wctg_settings_' . $args['section'] . '[' . $args['id'] . ']" value="1" ' . $checked . '/>';
	$html .= '<label for="rc_wctg_settings_' . $args['section'] . '[' . $args['id'] . ']"> '  . $args['desc'] . '</label>';

	echo $html;
}


/**
 * Multicheck Callback
 *
 * Renders multiple checkboxes.
 *
 * @access      private
 * @since       1.0
 * @return      void
*/

function rc_wctg_multicheck_callback($args) {
	global $rc_wctg_options;

	foreach( $args['options'] as $key => $option ):
		if( isset( $rc_wctg_options[$args['id']][$key] ) ) { $enabled = $option; } else { $enabled = NULL; }
		echo '<input name="rc_wctg_settings_' . $args['section'] . '[' . $args['id'] . '][' . $key . ']"" id="rc_wctg_settings_' . $args['section'] . '[' . $args['id'] . '][' . $key . ']" type="checkbox" value="' . $option . '" ' . checked($option, $enabled, false) . '/>&nbsp;';
		echo '<label for="rc_wctg_settings_' . $args['section'] . '[' . $args['id'] . '][' . $key . ']">' . $option . '</label><br/>';
	endforeach;
	echo '<p class="description">' . $args['desc'] . '</p>';
}


/**
 * Radio Callback
 *
 * Renders radio boxes.
 *
 * @access      private
 * @since       1.3.3
 * @return      void
*/

function rc_wctg_radio_callback($args) {

	global $rc_wctg_options;

	foreach($args['options'] as $key => $option) :
		$checked = false;
		if( isset( $rc_wctg_options[ $args['id'] ] ) && $rc_wctg_options[ $args['id'] ] == $key ) $checked = true;
		echo '<input name="rc_wctg_settings_' . $args['section'] . '[' . $args['id'] . ']"" id="rc_wctg_settings_' . $args['section'] . '[' . $args['id'] . '][' . $key . ']" type="radio" value="' . $key . '" ' . checked(true, $checked, false) . '/>&nbsp;';
		echo '<label for="rc_wctg_settings_' . $args['section'] . '[' . $args['id'] . '][' . $key . ']">' . $option . '</label><br/>';
	endforeach;
	echo '<p class="description">' . $args['desc'] . '</p>';

}


/**
 * Text Callback
 *
 * Renders text fields.
 *
 * @access      private
 * @since       1.0
 * @return      void
*/

function rc_wctg_text_callback($args) {
	global $rc_wctg_options;

	if( isset( $rc_wctg_options[ $args['id'] ] ) ) { $value = $rc_wctg_options[ $args['id'] ]; } else { $value = isset( $args['std'] ) ? $args['std'] : ''; }
	$size = isset( $args['size'] ) && !is_null($args['size']) ? $args['size'] : 'regular';
	$html = '<input type="text" class="' . $args['size'] . '-text" id="rc_wctg_settings_' . $args['section'] . '[' . $args['id'] . ']" name="rc_wctg_settings_' . $args['section'] . '[' . $args['id'] . ']" value="' . esc_attr( $value ) . '"/>';
	$html .= '<label for="rc_wctg_settings_' . $args['section'] . '[' . $args['id'] . ']"> '  . $args['desc'] . '</label>';

	echo $html;
}


/**
 * Textarea Callback
 *
 * Renders textarea fields.
 *
 * @access      private
 * @since       1.0
 * @return      void
*/

function rc_wctg_textarea_callback($args) {
	global $rc_wctg_options;

	if( isset( $rc_wctg_options[ $args['id'] ] ) ) { $value = $rc_wctg_options[ $args['id'] ]; } else { $value = isset( $args['std'] ) ? $args['std'] : ''; }
	$size = isset( $args['size'] ) && !is_null($args['size']) ? $args['size'] : 'regular';
	$html = '<textarea class="large-text" cols="50" rows="5" id="rc_wctg_settings_' . $args['section'] . '[' . $args['id'] . ']" name="rc_wctg_settings_' . $args['section'] . '[' . $args['id'] . ']">' . esc_textarea( $value ) . '</textarea>';
	$html .= '<label for="rc_wctg_settings_' . $args['section'] . '[' . $args['id'] . ']"> '  . $args['desc'] . '</label>';

	echo $html;
}


/**
 * Password Callback
 *
 * Renders password fields.
 *
 * @access      private
 * @since       1.3
 * @return      void
*/

function rc_wctg_password_callback($args) {
	global $rc_wctg_options;

	if( isset( $rc_wctg_options[ $args['id'] ] ) ) { $value = $rc_wctg_options[ $args['id'] ]; } else { $value = isset( $args['std'] ) ? $args['std'] : ''; }
	$size = isset( $args['size'] ) && !is_null($args['size']) ? $args['size'] : 'regular';
	$html = '<input type="password" class="' . $args['size'] . '-text" id="rc_wctg_settings_' . $args['section'] . '[' . $args['id'] . ']" name="rc_wctg_settings_' . $args['section'] . '[' . $args['id'] . ']" value="' . esc_attr( $value ) . '"/>';
	$html .= '<label for="rc_wctg_settings_' . $args['section'] . '[' . $args['id'] . ']"> '  . $args['desc'] . '</label>';

	echo $html;
}


/**
 * Missing Callback
 *
 * If a function is missing for settings callbacks alert the user.
 *
 * @access      private
 * @since       1.3.1
 * @return      void
*/

function rc_wctg_missing_callback($args) {
	printf( __( 'The callback function used for the <strong>%s</strong> setting is missing.', 'rc_wctg' ), $args['id'] );
}

/**
 * Select Callback
 *
 * Renders select fields.
 *
 * @access      private
 * @since       1.0
 * @return      void
*/

function rc_wctg_select_callback($args) {
	global $rc_wctg_options;

	$html = '<select id="rc_wctg_settings_' . $args['section'] . '[' . $args['id'] . ']" name="rc_wctg_settings_' . $args['section'] . '[' . $args['id'] . ']"/>';
	foreach( $args['options'] as $option => $name ) {
		$selected = isset( $rc_wctg_options[ $args['id'] ] ) ? selected( $option, $rc_wctg_options[$args['id']], false ) : '';
		$html .= '<option value="' . $option . '" ' . $selected . '>' . $name . '</option>';
	}
	$html .= '</select>';
	$html .= '<label for="rc_wctg_settings_' . $args['section'] . '[' . $args['id'] . ']"> '  . $args['desc'] . '</label>';

	echo $html;
}


/**
 * Rich Editor Callback
 *
 * Renders rich editor fields.
 *
 * @access      private
 * @since       1.0
 * @return      void
*/

function rc_wctg_rich_editor_callback($args) {
	global $rc_wctg_options, $wp_version;

	if( isset( $rc_wctg_options[ $args['id'] ] ) ) { $value = $rc_wctg_options[ $args['id'] ]; } else { $value = isset( $args['std'] ) ? $args['std'] : ''; }
	if( $wp_version >= 3.3 && function_exists('wp_editor')) {
		$html = wp_editor( $value, 'rc_wctg_settings_' . $args['section'] . '[' . $args['id'] . ']', array( 'textarea_name' => 'rc_wctg_settings_' . $args['section'] . '[' . $args['id'] . ']' ) );
	} else {
		$html = '<textarea class="large-text" rows="10" id="rc_wctg_settings_' . $args['section'] . '[' . $args['id'] . ']" name="rc_wctg_settings_' . $args['section'] . '[' . $args['id'] . ']">' . esc_textarea( $value ) . '</textarea>';
	}
	$html .= '<br/><label for="rc_wctg_settings_' . $args['section'] . '[' . $args['id'] . ']"> '  . $args['desc'] . '</label>';

	echo $html;
}


/**
 * Upload Callback
 *
 * Renders upload fields.
 *
 * @access      private
 * @since       1.0
 * @return      void
*/

function rc_wctg_upload_callback($args) {
	global $rc_wctg_options;

	if( isset( $rc_wctg_options[ $args['id'] ] ) ) { $value = $rc_wctg_options[$args['id']]; } else { $value = isset($args['std']) ? $args['std'] : ''; }
	$size = isset( $args['size'] ) && !is_null( $args['size'] ) ? $args['size'] : 'regular';
	$html = '<input type="text" class="' . $args['size'] . '-text rc_wctg_upload_field" id="rc_wctg_settings_' . $args['section'] . '[' . $args['id'] . ']" name="rc_wctg_settings_' . $args['section'] . '[' . $args['id'] . ']" value="' . esc_attr( $value ) . '"/>';
	$html .= '<span>&nbsp;<input type="button" class="rc_wctg_upload_image_button button-secondary" value="' . __('Upload File', 'rc_wctg') . '"/></span>';
	$html .= '<label for="rc_wctg_settings_' . $args['section'] . '[' . $args['id'] . ']"> '  . $args['desc'] . '</label>';

	echo $html;
}


/**
 * Hook Callback
 *
 * Adds a do_action() hook in place of the field
 *
 * @access      private
 * @since       1.0.8.2
 * @return      void
*/

function rc_wctg_hook_callback( $args ) {
	do_action( 'rc_wctg_' . $args['id'] );
}



/**
 * Settings Sanitization
 *
 * Adds a settings error (for the updated message)
 * At some point this will validate input
 *
 * @access      private
 * @since       1.0.8.2
 * @return      void
*/

function rc_wctg_settings_sanitize( $input ) {
	add_settings_error( 'rc_rmb-notices', '', __('Settings Updated', 'rc_wctg'), 'updated' );
	return $input;
}


/**
 * Get Settings
 *
 * Retrieves all plugin settings and returns them
 * as a combined array.
 *
 * @access      public
 * @since       1.0
 * @return      array
*/

function rc_wctg_get_settings() {
	$general_settings 	= is_array(get_option('rc_wctg_settings_general')) 	? get_option('rc_wctg_settings_general') 	: array();
	
	return array_merge($general_settings);
}