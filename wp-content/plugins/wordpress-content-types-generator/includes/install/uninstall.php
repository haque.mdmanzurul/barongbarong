<?php

// Exit if accessed directly
if ( !defined( 'ABSPATH' ) ) exit;

/**
 * Un-Install
 *
 * Runs on plugin uninstall.
 *
 * @access      private
 * @since       1.1
 * @return      void
*/

function rc_wctg_uninstall() {

	global $wpdb;
	global $post;
	
	delete_option( 'rc_wctg_is_installed' );
	delete_option( 'rc_wctg_settings_general' );

}

register_uninstall_hook( RC_WCTG_BASE_FILE, 'rc_wctg_uninstall' );