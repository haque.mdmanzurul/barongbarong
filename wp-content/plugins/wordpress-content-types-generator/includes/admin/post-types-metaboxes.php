<?php

function rc_wctg_cpt_save_postdata() {

    global $post;
    
    if ( isset($_POST['rc_wctg_submit']) && $_POST['rc_wctg_submit'] == 'true') {
    
    	/*
		|--------------------------------------------------------------------------
		| GET META FIELDS VALUES
		|--------------------------------------------------------------------------
		*/
		
    	// Mode
    	$rc_wctg_mode       		= get_post_meta($post->ID, 'rc_wctg_mode', true);
    	        
    	// General
        $rc_wctg_cpt_name           = get_post_meta($post->ID, 'rc_wctg_cpt_name', true);
        $rc_wctg_singular_name      = get_post_meta($post->ID, 'rc_wctg_singular_name', true);
        $rc_wctg_plural_name        = get_post_meta($post->ID, 'rc_wctg_plural_name', true);
        $rc_wctg_description        = get_post_meta($post->ID, 'rc_wctg_description', true);
        $rc_wctg_hierarchical       = get_post_meta($post->ID, 'rc_wctg_hierarchical', true);
        
        // Features
        $rc_wctg_s_title            = get_post_meta($post->ID, 'rc_wctg_s_title', true);
        $rc_wctg_s_editor           = get_post_meta($post->ID, 'rc_wctg_s_editor', true);
        $rc_wctg_s_author           = get_post_meta($post->ID, 'rc_wctg_s_author', true);
        $rc_wctg_s_thumbnail        = get_post_meta($post->ID, 'rc_wctg_s_thumbnail', true);
        $rc_wctg_s_excerpt          = get_post_meta($post->ID, 'rc_wctg_s_excerpt', true);
        $rc_wctg_s_trackbacks       = get_post_meta($post->ID, 'rc_wctg_s_trackbacks', true);
        $rc_wctg_s_custom_fields    = get_post_meta($post->ID, 'rc_wctg_s_custom_fields', true);
        $rc_wctg_s_comments         = get_post_meta($post->ID, 'rc_wctg_s_comments', true);
        $rc_wctg_s_revisions        = get_post_meta($post->ID, 'rc_wctg_s_revisions', true);
        $rc_wctg_s_post_formats     = get_post_meta($post->ID, 'rc_wctg_s_post_formats', true);
        $rc_wctg_categories         = get_post_meta($post->ID, 'rc_wctg_categories', true);
        $rc_wctg_tags       	    = get_post_meta($post->ID, 'rc_wctg_tags', true);
        $rc_wctg_page_categories    = get_post_meta($post->ID, 'rc_wctg_page_categories', true);
        
        // Labels
        $rc_wctg_add_new            = get_post_meta($post->ID, 'rc_wctg_add_new', true);
        $rc_wctg_add_new_item       = get_post_meta($post->ID, 'rc_wctg_add_new_item', true);
        $rc_wctg_edit_item          = get_post_meta($post->ID, 'rc_wctg_edit_item', true);
        $rc_wctg_new_item           = get_post_meta($post->ID, 'rc_wctg_new_item', true);
        $rc_wctg_all_items          = get_post_meta($post->ID, 'rc_wctg_all_items', true);
        $rc_wctg_view_item          = get_post_meta($post->ID, 'rc_wctg_view_item', true);
        $rc_wctg_search_items       = get_post_meta($post->ID, 'rc_wctg_search_items', true);
        $rc_wctg_not_found          = get_post_meta($post->ID, 'rc_wctg_not_found', true);
        $rc_wctg_not_found_in_trash = get_post_meta($post->ID, 'rc_wctg_not_found_in_trash', true);
        $rc_wctg_parent_item_colon  = get_post_meta($post->ID, 'rc_wctg_parent_item_colon', true);
        $rc_wctg_menu_text  		= get_post_meta($post->ID, 'rc_wctg_menu_text', true);
        
        // Visibility
        $rc_wctg_public             = get_post_meta($post->ID, 'rc_wctg_public', true);
        $rc_wctg_show_ui            = get_post_meta($post->ID, 'rc_wctg_show_ui', true);
        $rc_wctg_show_in_menu       = get_post_meta($post->ID, 'rc_wctg_show_in_menu', true); 
        $rc_wctg_show_in_menu_val   = get_post_meta($post->ID, 'rc_wctg_show_in_menu_val', true); 
        $rc_wctg_show_in_admin_bar  = get_post_meta($post->ID, 'rc_wctg_show_in_admin_bar', true); 
        $rc_wctg_exclude_from_search= get_post_meta($post->ID, 'rc_wctg_exclude_from_search', true); 
        $rc_wctg_show_in_nav_menus  = get_post_meta($post->ID, 'rc_wctg_show_in_nav_menus', true); 
        $rc_wctg_menu_position      = get_post_meta($post->ID, 'rc_wctg_menu_position', true);
        $rc_wctg_menu_icon      	= get_post_meta($post->ID, 'rc_wctg_menu_icon', true);
        
        // Options
        $rc_wctg_publicly_queryable = get_post_meta($post->ID, 'rc_wctg_publicly_queryable', true);
        $rc_wctg_query_var          = get_post_meta($post->ID, 'rc_wctg_query_var', true); 
        $rc_wctg_rewrite            = get_post_meta($post->ID, 'rc_wctg_rewrite', true); 
        $rc_wctg_has_archive        = get_post_meta($post->ID, 'rc_wctg_has_archive', true);
        $rc_wctg_can_export         = get_post_meta($post->ID, 'rc_wctg_can_export', true);
        
        // Capabilities
        $rc_wctg_capability_type           		 = get_post_meta($post->ID, 'rc_wctg_capability_type', true);
        $rc_wctg_advanced_capabilities           = get_post_meta($post->ID, 'rc_wctg_advanced_capabilities', true);
        $rc_wctg_capabilities_edit_post          = get_post_meta($post->ID, 'rc_wctg_capabilities_edit_post', true);
        $rc_wctg_capabilities_edit_posts         = get_post_meta($post->ID, 'rc_wctg_capabilities_edit_posts', true);
        $rc_wctg_capabilities_edit_others_posts  = get_post_meta($post->ID, 'rc_wctg_capabilities_edit_others_posts', true);
        $rc_wctg_capabilities_publish_posts      = get_post_meta($post->ID, 'rc_wctg_capabilities_publish_posts', true);
        $rc_wctg_capabilities_read_post          = get_post_meta($post->ID, 'rc_wctg_capabilities_read_post', true);
        $rc_wctg_capabilities_read_private_posts = get_post_meta($post->ID, 'rc_wctg_capabilities_read_private_posts', true);
        $rc_wctg_capabilities_delete_post        = get_post_meta($post->ID, 'rc_wctg_capabilities_delete_post', true);
        
        
		/*
		|--------------------------------------------------------------------------
		| UPDATE META FIELDS VALUES
		|--------------------------------------------------------------------------
		*/
		
        // Update Mode
        if( isset( $_POST['rc_wctg_mode'] ) ) { update_post_meta($post->ID, 'rc_wctg_mode', sanitize_title($_POST['rc_wctg_mode']), $rc_wctg_mode); }
        
        // Update General fields
        $rc_wctg_update_general_fields = array(
        							'rc_wctg_cpt_name', 
        							'rc_wctg_singular_name',
        							'rc_wctg_plural_name', 
        							'rc_wctg_description', 
        							'rc_wctg_hierarchical', 
        							);

        foreach( $rc_wctg_update_general_fields as $rc_wctg_update_general_field) {
        	if( isset( $_POST[$rc_wctg_update_general_field] ) ) { 
        		update_post_meta( $post->ID, $rc_wctg_update_general_field, $_POST[$rc_wctg_update_general_field] ); 
        	} else { 
        		delete_post_meta($post->ID, $rc_wctg_update_general_field); 
        	} // end if
        } // end foreach
        
        // Update Features
        $rc_wctg_update_features_fields = array(
        							'rc_wctg_s_title', 
        							'rc_wctg_s_editor',
        							'rc_wctg_s_author', 
        							'rc_wctg_s_thumbnail', 
        							'rc_wctg_s_excerpt', 
        							'rc_wctg_s_trackbacks', 
        							'rc_wctg_s_custom_fields', 
        							'rc_wctg_s_comments', 
        							'rc_wctg_s_revisions', 
        							'rc_wctg_s_post_formats', 
        							'rc_wctg_categories', 
        							'rc_wctg_tags', 
        							'rc_wctg_page_categories'
        							);

        foreach( $rc_wctg_update_features_fields as $rc_wctg_update_features_field) {
        	if( isset( $_POST[$rc_wctg_update_features_field] ) ) { 
        		update_post_meta( $post->ID, $rc_wctg_update_features_field, $_POST[$rc_wctg_update_features_field] ); 
        	} else { 
        		delete_post_meta($post->ID, $rc_wctg_update_features_field); 
        	} // end if
        } // end foreach
        
        // Update Labels
        $rc_wctg_update_labels_fields = array(
        							'rc_wctg_add_new', 
        							'rc_wctg_add_new_item',
        							'rc_wctg_edit_item', 
        							'rc_wctg_new_item', 
        							'rc_wctg_all_items', 
        							'rc_wctg_view_item', 
        							'rc_wctg_search_items', 
        							'rc_wctg_not_found', 
        							'rc_wctg_not_found_in_trash', 
        							'rc_wctg_parent_item_colon', 
        							'rc_wctg_menu_text', 
        							);

        foreach( $rc_wctg_update_labels_fields as $rc_wctg_update_labels_field) {
        	if( isset( $_POST[$rc_wctg_update_labels_field] ) ) { 
        		update_post_meta( $post->ID, $rc_wctg_update_labels_field, $_POST[$rc_wctg_update_labels_field] ); 
        	} else { 
        		delete_post_meta($post->ID, $rc_wctg_update_labels_field); 
        	} // end if
        } // end foreach

        // Update Visibility
        $rc_wctg_update_visibility_fields = array(
        							'rc_wctg_public', 
        							'rc_wctg_show_ui',
        							'rc_wctg_show_in_menu', 
        							'rc_wctg_show_in_menu_val', 
        							'rc_wctg_show_in_admin_bar', 
        							'rc_wctg_exclude_from_search', 
        							'rc_wctg_show_in_nav_menus', 
        							'rc_wctg_menu_position', 
        							'rc_wctg_menu_icon', 
        							);

        foreach( $rc_wctg_update_visibility_fields as $rc_wctg_update_visibility_field) {
        	if( isset( $_POST[$rc_wctg_update_visibility_field] ) ) { 
        		update_post_meta( $post->ID, $rc_wctg_update_visibility_field, $_POST[$rc_wctg_update_visibility_field] ); 
        	} else { 
        		delete_post_meta($post->ID, $rc_wctg_update_visibility_field); 
        	} // end if
        } // end foreach
        
        // Update Options
        $rc_wctg_update_options_fields = array(
        							'rc_wctg_publicly_queryable', 
        							'rc_wctg_query_var',
        							'rc_wctg_rewrite', 
        							'rc_wctg_has_archive', 
        							'rc_wctg_can_export', 
        							);

        foreach( $rc_wctg_update_options_fields as $rc_wctg_update_options_field) {
        	if( isset( $_POST[$rc_wctg_update_options_field] ) ) { 
        		update_post_meta( $post->ID, $rc_wctg_update_options_field, $_POST[$rc_wctg_update_options_field] ); 
        	} else { 
        		delete_post_meta($post->ID, $rc_wctg_update_options_field); 
        	} // end if
        } // end foreach

        // Update Capabilities
        $rc_wctg_update_capabilities_fields = array(
        							'rc_wctg_capability_type', 
        							'rc_wctg_advanced_capabilities',
        							'rc_wctg_capabilities_edit_post', 
        							'rc_wctg_capabilities_edit_posts', 
        							'rc_wctg_capabilities_edit_others_posts', 
        							'rc_wctg_capabilities_publish_posts', 
        							'rc_wctg_capabilities_read_post', 
        							'rc_wctg_capabilities_read_private_posts', 
        							'rc_wctg_capabilities_delete_post', 
        							);

        foreach( $rc_wctg_update_capabilities_fields as $rc_wctg_update_capabilities_field) {
        	if( isset( $_POST[$rc_wctg_update_capabilities_field] ) ) { 
        		update_post_meta( $post->ID, $rc_wctg_update_capabilities_field, $_POST[$rc_wctg_update_capabilities_field] ); 
        	} else { 
        		delete_post_meta($post->ID, $rc_wctg_update_capabilities_field); 
        	} // end if
        } // end foreach
        
        /*
        |--------------------------------------------------------------------------
        | BASIC MODE (forces values)
        |--------------------------------------------------------------------------
        */        
        if( isset( $_POST['rc_wctg_mode'] ) && $_POST['rc_wctg_mode'] == 'basic') {
	        update_post_meta($post->ID, 'rc_wctg_public', 'on' );
	        update_post_meta($post->ID, 'rc_wctg_show_ui', 'on' );
	        update_post_meta($post->ID, 'rc_wctg_show_in_menu', 'on' );
	        update_post_meta($post->ID, 'rc_wctg_publicly_queryable', 'on' );
        }
        
    }
}

function rc_wctg_cpt_metabox() {
    global $post;
    
    // Mode
    $rc_wctg_mode           	= get_post_meta($post->ID, 'rc_wctg_mode', true);
    
    // General
    $rc_wctg_cpt_name           = get_post_meta($post->ID, 'rc_wctg_cpt_name', true);
    $rc_wctg_singular_name      = get_post_meta($post->ID, 'rc_wctg_singular_name', true);
    $rc_wctg_plural_name        = get_post_meta($post->ID, 'rc_wctg_plural_name', true);
    $rc_wctg_description        = get_post_meta($post->ID, 'rc_wctg_description', true);
    $rc_wctg_hierarchical       = get_post_meta($post->ID, 'rc_wctg_hierarchical', true);

    // Features
    $rc_wctg_s_title            = get_post_meta($post->ID, 'rc_wctg_s_title', true);
    $rc_wctg_s_editor           = get_post_meta($post->ID, 'rc_wctg_s_editor', true);
    $rc_wctg_s_author           = get_post_meta($post->ID, 'rc_wctg_s_author', true);
    $rc_wctg_s_thumbnail        = get_post_meta($post->ID, 'rc_wctg_s_thumbnail', true);
    $rc_wctg_s_excerpt          = get_post_meta($post->ID, 'rc_wctg_s_excerpt', true);
    $rc_wctg_s_trackbacks       = get_post_meta($post->ID, 'rc_wctg_s_trackbacks', true);
    $rc_wctg_s_custom_fields    = get_post_meta($post->ID, 'rc_wctg_s_custom_fields', true);
    $rc_wctg_s_comments         = get_post_meta($post->ID, 'rc_wctg_s_comments', true);
    $rc_wctg_s_revisions        = get_post_meta($post->ID, 'rc_wctg_s_revisions', true);
    $rc_wctg_s_post_formats     = get_post_meta($post->ID, 'rc_wctg_s_post_formats', true);
    $rc_wctg_categories         = get_post_meta($post->ID, 'rc_wctg_categories', true);
    $rc_wctg_tags               = get_post_meta($post->ID, 'rc_wctg_tags', true);
    $rc_wctg_page_categories    = get_post_meta($post->ID, 'rc_wctg_page_categories', true);

    // Labels
    $rc_wctg_add_new            = get_post_meta($post->ID, 'rc_wctg_add_new', true);
    $rc_wctg_add_new_item       = get_post_meta($post->ID, 'rc_wctg_add_new_item', true);
    $rc_wctg_edit_item          = get_post_meta($post->ID, 'rc_wctg_edit_item', true);
    $rc_wctg_new_item           = get_post_meta($post->ID, 'rc_wctg_new_item', true);
    $rc_wctg_all_items          = get_post_meta($post->ID, 'rc_wctg_all_items', true);
    $rc_wctg_view_item          = get_post_meta($post->ID, 'rc_wctg_view_item', true);
    $rc_wctg_search_items       = get_post_meta($post->ID, 'rc_wctg_search_items', true);
    $rc_wctg_not_found          = get_post_meta($post->ID, 'rc_wctg_not_found', true);
    $rc_wctg_not_found_in_trash = get_post_meta($post->ID, 'rc_wctg_not_found_in_trash', true);
    $rc_wctg_parent_item_colon  = get_post_meta($post->ID, 'rc_wctg_parent_item_colon', true);
    $rc_wctg_menu_text  		= get_post_meta($post->ID, 'rc_wctg_menu_text', true);

    // Visibility
    $rc_wctg_public             = get_post_meta($post->ID, 'rc_wctg_public', true);
    $rc_wctg_show_ui            = get_post_meta($post->ID, 'rc_wctg_show_ui', true);
    $rc_wctg_show_in_menu       = get_post_meta($post->ID, 'rc_wctg_show_in_menu', true); 
    $rc_wctg_show_in_menu_val       = get_post_meta($post->ID, 'rc_wctg_show_in_menu_val', true); 
    $rc_wctg_show_in_admin_bar       = get_post_meta($post->ID, 'rc_wctg_show_in_admin_bar', true); 
    $rc_wctg_exclude_from_search       = get_post_meta($post->ID, 'rc_wctg_exclude_from_search', true); 
    $rc_wctg_show_in_nav_menus       = get_post_meta($post->ID, 'rc_wctg_show_in_nav_menus', true); 
    $rc_wctg_menu_position      = get_post_meta($post->ID, 'rc_wctg_menu_position', true);
    $rc_wctg_menu_icon      	= get_post_meta($post->ID, 'rc_wctg_menu_icon', true);
    
    // Options
    $rc_wctg_publicly_queryable = get_post_meta($post->ID, 'rc_wctg_publicly_queryable', true);
    $rc_wctg_query_var          = get_post_meta($post->ID, 'rc_wctg_query_var', true); 
    $rc_wctg_rewrite            = get_post_meta($post->ID, 'rc_wctg_rewrite', true); 
    $rc_wctg_has_archive        = get_post_meta($post->ID, 'rc_wctg_has_archive', true); 
    $rc_wctg_can_export        = get_post_meta($post->ID, 'rc_wctg_can_export', true); 
    
    // Capabilities
    $rc_wctg_capability_type          		 = get_post_meta($post->ID, 'rc_wctg_capability_type', true);
    $rc_wctg_advanced_capabilities           = get_post_meta($post->ID, 'rc_wctg_advanced_capabilities', true);
    $rc_wctg_capabilities_edit_post          = get_post_meta($post->ID, 'rc_wctg_capabilities_edit_post', true);
    $rc_wctg_capabilities_edit_posts         = get_post_meta($post->ID, 'rc_wctg_capabilities_edit_posts', true);
    $rc_wctg_capabilities_edit_others_posts  = get_post_meta($post->ID, 'rc_wctg_capabilities_edit_others_posts', true);
    $rc_wctg_capabilities_publish_posts      = get_post_meta($post->ID, 'rc_wctg_capabilities_publish_posts', true);
    $rc_wctg_capabilities_read_post          = get_post_meta($post->ID, 'rc_wctg_capabilities_read_post', true);
    $rc_wctg_capabilities_read_private_posts = get_post_meta($post->ID, 'rc_wctg_capabilities_read_private_posts', true);
    $rc_wctg_capabilities_delete_post        = get_post_meta($post->ID, 'rc_wctg_capabilities_delete_post', true);
    
    ?>

    <?php if( isset($rc_wctg_mode) && $rc_wctg_mode == 'basic') : ?>    
    <script language="javascript">
	    jQuery(document).ready(function($) {
			$("#rc_wctg_general").show();
			$("#rc_wctg_features").hide();
			$("#rc_wctg_labels").hide();
			$("#rc_wctg_visibility").hide();
			$("#rc_wctg_options").hide();
			$("#rc_wctg_capabilities").hide();
			
			$("#rc_wctg_next_button_features").hide();
			
			$("#rc_wctg_tab_labels").hide();
			$("#rc_wctg_tab_visibility").hide();
			$("#rc_wctg_tab_options").hide();
			$("#rc_wctg_tab_capabilities").hide();
			
		});
    </script>
    <?php endif; ?>
    
<ol class="rc_wctg_step_tracker" data-rc_wctg_step_tracker-steps="5">
    <li class="rc_wctg_step_tracker-done" id="rc_wctg_tab_general">1 &rsaquo; <a href="#"><?php _e('General', 'rc_wctg'); ?></a></li><!--
 --><li class="rc_wctg_step_tracker-todo" id="rc_wctg_tab_features">2 &rsaquo; <a href="#"><?php _e('Features', 'rc_wctg'); ?></a></li><!--
 --><li class="rc_wctg_step_tracker-todo" id="rc_wctg_tab_labels">3 &rsaquo; <a href="#"><?php _e('Labels', 'rc_wctg'); ?></a></li><!--
 --><li class="rc_wctg_step_tracker-todo" id="rc_wctg_tab_visibility">4 &rsaquo; <a href="#"><?php _e('Visibility', 'rc_wctg'); ?></a></li><!--
 --><li class="rc_wctg_step_tracker-todo" id="rc_wctg_tab_options">5 &rsaquo; <a href="#"><?php _e('Options', 'rc_wctg'); ?></a></li><!--
 --><li class="rc_wctg_step_tracker-todo" id="rc_wctg_tab_capabilities">6 &rsaquo; <a href="#"><?php _e('Capabilities', 'rc_wctg'); ?></a></li>
</ol>

	<?php
	/*
	|--------------------------------------------------------------------------
	| GENERAL
	|--------------------------------------------------------------------------
	*/
	?>
    <div id="rc_wctg_general" class="rc_wctg_general">
    
	    <h4><?php _e('STEP', 'rc_wctg'); ?> 1: <?php _e('GENERAL PARAMETERS', 'rc_wctg'); ?></h4>
	    
	    <table width="100%" class="rc_wctg_cpt_fields">
	        <tr>
	            <td>
	            	<?php _e('Post Type Key', 'rc_wctg'); ?><span class="required">*</span>:<br/> <input type="text" name="rc_wctg_cpt_name" id="rc_wctg_cpt_name" value="<?php echo $rc_wctg_cpt_name; ?>"/>
	            	<span class="description"><?php _e('The CPT key, ex: book', 'rc_wctg'); ?></span>
	            </td>
	            <td>
	            	<?php _e('Singular Label', 'rc_wctg'); ?><span class="required">*</span>:<br/> <input type="text" name="rc_wctg_singular_name" id="rc_wctg_singular_name" value="<?php echo $rc_wctg_singular_name; ?>"/>
	            	<span class="description"><?php _e('The singular name for your CPT, ex: Book', 'rc_wctg'); ?></span>
	            </td>
	            <td>
	            	<?php _e('Plural Label', 'rc_wctg'); ?><span class="required">*</span>:<br/> <input type="text" name="rc_wctg_plural_name" id="rc_wctg_plural_name" value="<?php echo $rc_wctg_plural_name; ?>"/>
	            	<span class="description"><?php _e('The plural name for your CPT, ex: Books', 'rc_wctg'); ?></span>
	            </td>
	        </tr>
	        <tr>
	            <td colspan="3">
	            	<?php _e('Description', 'rc_wctg'); ?>:<br/> <textarea name="rc_wctg_description" /><?php echo $rc_wctg_description; ?></textarea>
	            	<span class="description"><?php _e('A short descriptive summary of what the post type is', 'rc_wctg'); ?></span>
	            </td>
	        </tr>
	        <tr>
	            <td colspan="3"><input type="checkbox" name="rc_wctg_hierarchical" <?php checked( $rc_wctg_hierarchical, 'on' ); ?>/> <?php _e('Hierarchical', 'rc_wctg'); ?> <span class="description"><?php _e('Whether the post type is hierarchical (arranged in a tree-like structure). If checked, then a parent can be specified for each custom post type. Ex: pages are hierarchical, posts are non-hierarchical', 'rc_wctg'); ?></span></td>
	        </tr>
	    </table>
	    
	    <div class="rc_wctg_prev_next_buttons">
	    	<input type="button" id="rc_wctg_next_button_general" class="button button-primary button-large" value="<?php _e('Next', 'rc_wctg'); ?>">
	    </div>
    </div>
    
	<?php
	/*
	|--------------------------------------------------------------------------
	| Features
	|--------------------------------------------------------------------------
	*/
	?>
	<div id="rc_wctg_features" class="rc_wctg_features">

		<h4><?php _e('STEP', 'rc_wctg'); ?> 2: <?php _e('SUPPORTED FEATURES', 'rc_wctg'); ?></h4>
		
	    <table width="100%" class="rc_wctg_cpt_fields">
	        <tr>
	            <td><input type="checkbox" name="rc_wctg_s_title" <?php checked( $rc_wctg_s_title, 'on' ); ?>/> <?php _e('Title', 'rc_wctg'); ?><span class="required">*</span></td>
	            <td><input type="checkbox" name="rc_wctg_s_editor" <?php checked( $rc_wctg_s_editor, 'on' ); ?>/> <?php _e('Editor', 'rc_wctg'); ?></td>
	            <td><input type="checkbox" name="rc_wctg_s_author" <?php checked( $rc_wctg_s_author, 'on' ); ?>/> <?php _e('Author', 'rc_wctg'); ?></td> 
	            <td><input type="checkbox" name="rc_wctg_s_thumbnail" <?php checked( $rc_wctg_s_thumbnail, 'on' ); ?>/> <?php _e('Thumbnail', 'rc_wctg'); ?></td>
	            <td><input type="checkbox" name="rc_wctg_s_excerpt" <?php checked( $rc_wctg_s_excerpt, 'on' ); ?>/> <?php _e('Excerpt', 'rc_wctg'); ?></td>
	            <td><input type="checkbox" name="rc_wctg_s_trackbacks" <?php checked( $rc_wctg_s_trackbacks, 'on' ); ?>/> <?php _e('Trackbacks', 'rc_wctg'); ?></td>
	            <td><input type="checkbox" name="rc_wctg_s_custom_fields" <?php checked( $rc_wctg_s_custom_fields, 'on' ); ?>/> <?php _e('Custom Fields', 'rc_wctg'); ?></td>
	            <td><input type="checkbox" name="rc_wctg_s_comments" <?php checked( $rc_wctg_s_comments, 'on' ); ?>/> <?php _e('Comments', 'rc_wctg'); ?></td>
	            <td><input type="checkbox" name="rc_wctg_s_revisions" <?php checked( $rc_wctg_s_revisions, 'on' ); ?>/> <?php _e('Revisions', 'rc_wctg'); ?></td>
	            <td><input type="checkbox" name="rc_wctg_s_post_formats" <?php checked( $rc_wctg_s_post_formats, 'on' ); ?>/> <?php _e('Post Formats', 'rc_wctg'); ?></td>
	        </tr>
	    </table>
	    
	    <span class="description"><?php _e('Choose the CPT supported features. You should at least check the title field. You can modify these values later while CPT edition', 'rc_wctg'); ?></span>
	    
	    <h4><?php _e('TAGS AND CATEGORIES', 'rc_wctg'); ?></h4>
	    
	    <table width="100%" class="rc_wctg_cpt_fields">
	        <tr>
	            <td>
	            	<input type="checkbox" name="rc_wctg_categories" <?php checked( $rc_wctg_categories, 'on' ); ?>/> <?php _e('Categories', 'rc_wctg'); ?><br />
	            	<input type="checkbox" name="rc_wctg_tags" <?php checked( $rc_wctg_tags, 'on' ); ?>/> <?php _e('Tags', 'rc_wctg'); ?>	<br />
	            	<input type="checkbox" name="rc_wctg_page_categories" <?php checked( $rc_wctg_page_categories, 'on' ); ?>/> <?php _e('Page categories', 'rc_wctg'); ?>
	            </td>
	        </tr>
	    </table>
	    
	    <span class="description"><?php _e('Which registered taxonomies will be associated to this post type. The taxonomy meta boxes will be visible when managing this post type', 'rc_wctg'); ?></span>
	
	    <div class="rc_wctg_prev_next_buttons">
	    	<input type="button" id="rc_wctg_previous_button_features" class="button button button-large" value="<?php _e('Previous', 'rc_wctg'); ?>">
	    	<input type="button" id="rc_wctg_next_button_features" class="button button-primary button-large" value="<?php _e('Next', 'rc_wctg'); ?>">
	    </div>
	</div>
    
	<?php
	/*
	|--------------------------------------------------------------------------
	| Labels
	|--------------------------------------------------------------------------
	*/
	?>
	<div id="rc_wctg_labels" class="rc_wctg_labels">

	    <h4><?php _e('STEP', 'rc_wctg'); ?> 3: <?php _e('LABELS', 'rc_wctg'); ?></h4>
	    
	    <table width="100%" class="rc_wctg_cpt_fields">
	        <tr>
	            <td><?php _e('Add new', 'rc_wctg'); ?>:<br/> <input type="text" name="rc_wctg_add_new" id="rc_wctg_add_new" value="<?php echo $rc_wctg_add_new; ?>"/></td>
	            <td><?php _e('Add new item', 'rc_wctg'); ?>:<br/> <input type="text" name="rc_wctg_add_new_item" id="rc_wctg_add_new_item" value="<?php echo $rc_wctg_add_new_item; ?>"/></td>
	            <td><?php _e('New Item', 'rc_wctg'); ?>:<br/> <input type="text" name="rc_wctg_new_item" id="rc_wctg_new_item" value="<?php echo $rc_wctg_new_item; ?>"/></td>
	        </tr>
	        <tr>
	            <td><?php _e('Edit Item', 'rc_wctg'); ?>:<br/> <input type="text" name="rc_wctg_edit_item" id="rc_wctg_edit_item" value="<?php echo $rc_wctg_edit_item; ?>"/></td>
	            <td><?php _e('All Items', 'rc_wctg'); ?>:<br/> <input type="text" name="rc_wctg_all_items" id="rc_wctg_all_items" value="<?php echo $rc_wctg_all_items; ?>"/></td>
	            <td><?php _e('View Item', 'rc_wctg'); ?>:<br/> <input type="text" name="rc_wctg_view_item" id="rc_wctg_view_item" value="<?php echo $rc_wctg_view_item; ?>"/></td>
	        </tr>
	        <tr>
	            <td><?php _e('Search Items', 'rc_wctg'); ?>:<br/> <input type="text" name="rc_wctg_search_items" id="rc_wctg_search_items" value="<?php echo $rc_wctg_search_items; ?>"/></td>
	            <td><?php _e('Not Found', 'rc_wctg'); ?>:<br/> <input type="text" name="rc_wctg_not_found" id="rc_wctg_not_found" value="<?php echo $rc_wctg_not_found; ?>"/></td>
	            <td><?php _e('Not Found in Trash', 'rc_wctg'); ?>:<br/> <input type="text" name="rc_wctg_not_found_in_trash" id="rc_wctg_not_found_in_trash" value="<?php echo $rc_wctg_not_found_in_trash; ?>"/></td>
	        </tr>
	        <tr>
	            <td><?php _e('Parent item Column', 'rc_wctg'); ?>:<br/> <input type="text" name="rc_wctg_parent_item_colon" id="rc_wctg_parent_item_colon" value="<?php echo $rc_wctg_parent_item_colon; ?>"/></td>
	            <td><?php _e('Menu Text', 'rc_wctg'); ?>:<br/> <input type="text" name="rc_wctg_menu_text" id="rc_wctg_menu_text" value="<?php echo $rc_wctg_menu_text; ?>"/></td>
	        </tr>
	    </table>
	    
	    <span class="description"><?php _e('Labels are texts used for each action related to the CPT', 'rc_wctg'); ?></span>
	
	    <div class="rc_wctg_prev_next_buttons">
	   		<input type="button" id="rc_wctg_previous_button_labels" class="button button button-large" value="<?php _e('Previous', 'rc_wctg'); ?>">
	   		<input type="button" id="rc_wctg_next_button_labels" class="button button-primary button-large" value="<?php _e('Next', 'rc_wctg'); ?>">
	    </div>
	</div>
	
	<?php
	/*
	|--------------------------------------------------------------------------
	| Visibility
	|--------------------------------------------------------------------------
	*/
	?>
	<div id="rc_wctg_visibility" class="rc_wctg_visibility">

	    <h4><?php _e('STEP', 'rc_wctg'); ?> 4: <?php _e('VISIBILITY', 'rc_wctg'); ?></h4>
	    
	    <table width="100%" class="rc_wctg_cpt_fields">
	        <tr>
	            <td><input type="checkbox" name="rc_wctg_public" <?php checked( $rc_wctg_public, 'on' ); ?>/> <?php _e('Public', 'rc_wctg'); ?></td>
	            <td><input type="checkbox" name="rc_wctg_show_ui" <?php checked( $rc_wctg_show_ui, 'on' ); ?>/> <?php _e('Show UI', 'rc_wctg'); ?></td>
	            <td><input type="checkbox" name="rc_wctg_exclude_from_search" <?php checked( $rc_wctg_exclude_from_search, 'on' ); ?>/> <?php _e('Exclude from search', 'rc_wctg'); ?></td>
	            <td><input type="checkbox" name="rc_wctg_show_in_nav_menus" <?php checked( $rc_wctg_show_in_nav_menus, 'on' ); ?>/> <?php _e('Show in Nav Menus', 'rc_wctg'); ?></td>
	            <td><input type="checkbox" name="rc_wctg_show_in_admin_bar" <?php checked( $rc_wctg_show_in_admin_bar, 'on' ); ?>/> <?php _e('Show in Admin Bar', 'rc_wctg'); ?></td>
	        </tr>
	    </table>
	    
	    <span class="description"><?php _e('Define if the CPT is public or not and if it should be integrated in the main UI', 'rc_wctg'); ?></span>
	    
	    <h4><?php _e('SHOW IN MENU', 'rc_wctg'); ?></h4>
	    
	    <table width="100%" class="rc_wctg_cpt_fields">
	        <tr>
	            <td><input type="checkbox" name="rc_wctg_show_in_menu" <?php checked( $rc_wctg_show_in_menu, 'on' ); ?>/> <?php _e('Show in Menu', 'rc_wctg'); ?></td>
	            <td><?php _e('Custom Parent Menu', 'rc_wctg'); ?>: <input type="text" name="rc_wctg_show_in_menu_val" /></td>
	        </tr>
	    </table>
	    
	    <span class="description"><?php _e('Choose to integrate the CPT in the main left menu. You can specific a custom page in "custom parent menu" field', 'rc_wctg'); ?></span>
	    
	    <h4><?php _e('MENU POSITION', 'rc_wctg'); ?></h4>
	    
	    <table width="100%" class="rc_wctg_cpt_fields">
			<td>
				<?php _e('Menu Position', 'rc_wctg'); ?>: 
				<select name="rc_wctg_menu_position">
					<option value="">-- <?php _e('choose', 'rc_wctg'); ?> --</option>
					<option value="5" <?php selected( $rc_wctg_menu_position, 5 ); ?>><?php _e('below', 'rc_wctg'); ?> <?php _e('Posts', 'rc_wctg'); ?></option>
					<option value="10" <?php selected( $rc_wctg_menu_position, 10 ); ?>><?php _e('below', 'rc_wctg'); ?> <?php _e('Media', 'rc_wctg'); ?></option>
					<option value="15" <?php selected( $rc_wctg_menu_position, 15 ); ?>><?php _e('below', 'rc_wctg'); ?> <?php _e('Links', 'rc_wctg'); ?></option>
					<option value="20" <?php selected( $rc_wctg_menu_position, 20 ); ?>><?php _e('below', 'rc_wctg'); ?> <?php _e('Pages', 'rc_wctg'); ?></option>
					<option value="25" <?php selected( $rc_wctg_menu_position, 25 ); ?>><?php _e('below', 'rc_wctg'); ?> <?php _e('comments', 'rc_wctg'); ?></option>
					<option value="60" <?php selected( $rc_wctg_menu_position, 60 ); ?>><?php _e('below', 'rc_wctg'); ?> <?php _e('First separator', 'rc_wctg'); ?></option>
					<option value="65" <?php selected( $rc_wctg_menu_position, 65 ); ?>><?php _e('below', 'rc_wctg'); ?> <?php _e('Plugins', 'rc_wctg'); ?></option>
					<option value="70" <?php selected( $rc_wctg_menu_position, 70 ); ?>><?php _e('below', 'rc_wctg'); ?> <?php _e('Users', 'rc_wctg'); ?></option>
					<option value="75" <?php selected( $rc_wctg_menu_position, 75 ); ?>><?php _e('below', 'rc_wctg'); ?> <?php _e('Tools', 'rc_wctg'); ?></option>
					<option value="80" <?php selected( $rc_wctg_menu_position, 80 ); ?>><?php _e('below', 'rc_wctg'); ?> <?php _e('Settings', 'rc_wctg'); ?></option>
					<option value="100" <?php selected( $rc_wctg_menu_position, 100 ); ?>><?php _e('below', 'rc_wctg'); ?> <?php _e('Second separator', 'rc_wctg'); ?></option>
				</select>
			</td>
		</table>
	    
	    <span class="description"><?php _e('Choose the location to display the CPT in the main left WordPress menu', 'rc_wctg'); ?></span>
	    
	    <h4><?php _e('MENU ICON', 'rc_wctg'); ?></h4>
	    
	    <table width="100%" class="rc_wctg_cpt_fields">
	        <tr>
	            <td><?php _e('Menu Icon', 'rc_wctg'); ?>:<br/> <input type="text" class="rc_wctg_upload_image" id="rc_wctg_menu_icon" name="rc_wctg_menu_icon" value="<?php echo $rc_wctg_menu_icon; ?>"/> <input type="button" class="rc_wctg_upload_image_button button-secondary" value="<?php _e('Upload File', 'rc_wctg'); ?>"/></td>
	        </tr>
	    </table>
	    
	    <span class="description"><?php _e('Choose an icon to be displayed in the main menu. Recommended size is 16x16px', 'rc_wctg'); ?></span>
    
	    <div class="rc_wctg_prev_next_buttons">
	    	<input type="button" id="rc_wctg_previous_button_visibility" class="button button button-large" value="<?php _e('Previous', 'rc_wctg'); ?>">
	    	<input type="button" id="rc_wctg_next_button_visibility" class="button button-primary button-large" value="<?php _e('Next', 'rc_wctg'); ?>">
	    </div>
	</div>
	
	<?php
	/*
	|--------------------------------------------------------------------------
	| Options
	|--------------------------------------------------------------------------
	*/
	?>
	<div id="rc_wctg_options" class="rc_wctg_options">

	    <h4><?php _e('STEP', 'rc_wctg'); ?> 5: <?php _e('OPTIONS', 'rc_wctg'); ?></h4>
	    
	    <table width="100%" class="rc_wctg_cpt_fields">
	        <tr>
	            <td><input type="checkbox" name="rc_wctg_publicly_queryable" <?php checked( $rc_wctg_publicly_queryable, 'on' ); ?>/> <?php _e('Publicly Queryable', 'rc_wctg'); ?></td>
	            <td><input type="checkbox" name="rc_wctg_query_var" <?php checked( $rc_wctg_query_var, 'on' ); ?>/> <?php _e('Query Var', 'rc_wctg'); ?></td>
	            <td><input type="checkbox" name="rc_wctg_rewrite" <?php checked( $rc_wctg_rewrite, 'on' ); ?>/> <?php _e('Rewrite', 'rc_wctg'); ?></td>
	            <td><input type="checkbox" name="rc_wctg_has_archive" <?php checked( $rc_wctg_has_archive, 'on' ); ?>/> <?php _e('Has Archive', 'rc_wctg'); ?></td>
	            <td><input type="checkbox" name="rc_wctg_can_export" <?php checked( $rc_wctg_can_export, 'on' ); ?>/> <?php _e('Can export', 'rc_wctg'); ?></td>
	        </tr>
	    </table>
	    
	    <span class="description"><?php _e('These are advanced options, if you are not familiar with them, simply leave blank, and click next', 'rc_wctg'); ?></span>

	    <div class="rc_wctg_prev_next_buttons">
	    	<input type="button" id="rc_wctg_previous_button_options" class="button button button-large" value="<?php _e('Previous', 'rc_wctg'); ?>">
	    	<input type="button" id="rc_wctg_next_button_options" class="button button-primary button-large" value="<?php _e('Next', 'rc_wctg'); ?>">
	    </div>
	</div>
	
	<?php
	/*
	|--------------------------------------------------------------------------
	| Capabilities
	|--------------------------------------------------------------------------
	*/
	?>
	<div id="rc_wctg_capabilities" class="rc_wctg_capabilities">

	    <h4><?php _e('STEP', 'rc_wctg'); ?> 6:<?php _e('CAPABILITIES', 'rc_wctg'); ?></h4>
	    
	    <table width="100%" class="rc_wctg_cpt_fields">
			<td>
				<?php _e('Capability Type', 'rc_wctg'); ?>: 
				<select name="rc_wctg_capability_type">
					<option value="page" <?php selected( $rc_wctg_capability_type, 'page' ); ?>> <?php _e('page', 'rc_wctg'); ?></option>
					<option value="post" <?php selected( $rc_wctg_capability_type, 'post' ); ?>> <?php _e('post', 'rc_wctg'); ?></option>
				</select>
			</td>
		</table>
	    
	    <span class="description"><?php _e('Choose to use a set of custom predefined capabilities.', 'rc_wctg'); ?></span>
	    
	    <h4><?php _e('ADVANCED CAPABILITIES', 'rc_wctg'); ?></h4>
	    
	    <table width="100%" class="rc_wctg_cpt_fields">
			<td><input type="checkbox" name="rc_wctg_advanced_capabilities" id="rc_wctg_advanced_capabilities" <?php checked( $rc_wctg_advanced_capabilities, 'on' ); ?>/> <?php _e('Use Advanced Capabilities', 'rc_wctg'); ?></td>
		</table>
		
		<span class="description"><?php _e('Check to define your own capabilities for the CPT.', 'rc_wctg'); ?></span>
		
		<h4><?php _e('CUSTOM CAPABILITIES', 'rc_wctg'); ?></h4>
		
	    <table width="100%" class="rc_wctg_cpt_fields">
	        <tr>
	            <td><?php _e('Edit Post', 'rc_wctg'); ?>:<br/> <input type="text" name="rc_wctg_capabilities_edit_post" id="rc_wctg_capabilities_edit_post" value="<?php echo $rc_wctg_capabilities_edit_post; ?>"/></td>
	            <td><?php _e('Edit Posts', 'rc_wctg'); ?>:<br/> <input type="text" name="rc_wctg_capabilities_edit_posts" id="rc_wctg_capabilities_edit_posts" value="<?php echo $rc_wctg_capabilities_edit_posts; ?>"/></td>
	            <td><?php _e('Edit Others Posts', 'rc_wctg'); ?>:<br/> <input type="text" name="rc_wctg_capabilities_edit_others_posts" id="rc_wctg_capabilities_edit_others_posts" value="<?php echo $rc_wctg_capabilities_edit_others_posts; ?>"/></td>
	        </tr>
	        <tr>
	            <td><?php _e('Publish Posts', 'rc_wctg'); ?>:<br/> <input type="text" name="rc_wctg_capabilities_publish_posts" id="rc_wctg_capabilities_publish_posts" value="<?php echo $rc_wctg_capabilities_publish_posts; ?>"/></td>
	            <td><?php _e('Read Post', 'rc_wctg'); ?>:<br/> <input type="text" name="rc_wctg_capabilities_read_post" id="rc_wctg_capabilities_read_post" value="<?php echo $rc_wctg_capabilities_read_post; ?>"/></td>
	            <td><?php _e('Read Private Posts', 'rc_wctg'); ?>:<br/> <input type="text" name="rc_wctg_capabilities_read_private_posts" id="rc_wctg_capabilities_read_private_posts" value="<?php echo $rc_wctg_capabilities_read_private_posts; ?>"/></td>
	        </tr>
	        <tr>
	            <td><?php _e('Delete Post', 'rc_wctg'); ?>:<br/> <input type="text" name="rc_wctg_capabilities_delete_post" id="rc_wctg_capabilities_delete_post" value="<?php echo $rc_wctg_capabilities_delete_post; ?>"/></td>
	        </tr>
	    </table>

	    <div class="rc_wctg_prev_next_buttons">
	    	<input type="button" id="rc_wctg_previous_button_capabilities" class="button button button-large" value="<?php _e('Previous', 'rc_wctg'); ?>">
	    </div>
	</div>
    

    <input type="hidden" name="rc_wctg_submit" value="true" />
    <?php
}


function rc_wctg_cpt_export_metabox() {
    global $post;
    
        /*
        |--------------------------------------------------------------------------
        | General
        |--------------------------------------------------------------------------
        */
        $rc_wctg_cpt_name           = get_post_meta($post->ID, 'rc_wctg_cpt_name', true);
        $rc_wctg_singular_name      = get_post_meta($post->ID, 'rc_wctg_singular_name', true);
        $rc_wctg_plural_name        = get_post_meta($post->ID, 'rc_wctg_plural_name', true);
        $rc_wctg_description        = get_post_meta($post->ID, 'rc_wctg_description', true);
        $rc_wctg_hierarchical       = get_post_meta($post->ID, 'rc_wctg_hierarchical', true);
        
        /*
        |--------------------------------------------------------------------------
        | FEATURES
        |--------------------------------------------------------------------------
        */
        $rc_wctg_s = array();
        $rc_wctg_taxos = array();
        
        $rc_wctg_s_title = get_post_meta($post->ID, 'rc_wctg_s_title', true);
        if ($rc_wctg_s_title == "on") {
            $rc_wctg_s[] = 'title';
        }
        $rc_wctg_s_editor = get_post_meta($post->ID, 'rc_wctg_s_editor', true);
        if ($rc_wctg_s_editor == "on") {
            $rc_wctg_s[] = 'editor';
        }
        $rc_wctg_s_author = get_post_meta($post->ID, 'rc_wctg_s_author', true);
        if ($rc_wctg_s_author == "on") {
            $rc_wctg_s[] = 'author';
        }
        $rc_wctg_s_thumbnail = get_post_meta($post->ID, 'rc_wctg_s_thumbnail', true);
        if ($rc_wctg_s_thumbnail == "on") {
            $rc_wctg_s[] = 'thumbnail';
        }
        $rc_wctg_s_excerpt = get_post_meta($post->ID, 'rc_wctg_s_excerpt', true);
        if ($rc_wctg_s_excerpt == "on") {
            array_push($rc_wctg_s, 'excerpt');
        }
        $rc_wctg_s_trackbacks = get_post_meta($post->ID, 'rc_wctg_s_trackbacks', true);
        if ($rc_wctg_s_trackbacks == "on") {
            array_push($rc_wctg_s, 'trackbacks');
        }
        $rc_wctg_s_custom_fields = get_post_meta($post->ID, 'rc_wctg_s_custom_fields', true);
        if ($rc_wctg_s_custom_fields == "on") {
            array_push($rc_wctg_s, 'custom-fields');
        }
        $rc_wctg_s_comments = get_post_meta($post->ID, 'rc_wctg_s_comments', true);
        if ($rc_wctg_s_comments == "on") {
            array_push($rc_wctg_s, 'comments');
        }
        $rc_wctg_s_revisions = get_post_meta($post->ID, 'rc_wctg_s_revisions', true);
        if ($rc_wctg_s_revisions == "on") {
            array_push($rc_wctg_s, 'revisions');
        }
        $rc_wctg_s_post_formats = get_post_meta($post->ID, 'rc_wctg_s_post_formats', true);
        if ($rc_wctg_s_post_formats == "on") {
            array_push($rc_wctg_s, 'post-formats');
        }
        
        $rc_wctg_categories = get_post_meta($post->ID, 'rc_wctg_categories', true);
        if ($rc_wctg_categories == "on") {
            $rc_wctg_taxos[] = 'category';
        }
        $rc_wctg_tags = get_post_meta($post->ID, 'rc_wctg_tags', true);
        if ($rc_wctg_tags == "on") {
            $rc_wctg_taxos[] = 'post_tag';
        }
        $rc_wctg_page_categories = get_post_meta($post->ID, 'rc_wctg_page_categories', true);
        if ($rc_wctg_page_categories == "on") {
            $rc_wctg_taxos[] = 'page-category';
        }
        
        /*
        |--------------------------------------------------------------------------
        | LABELS
        |--------------------------------------------------------------------------
        */

        $rc_wctg_add_new            = get_post_meta($post->ID, 'rc_wctg_add_new', true);
        $rc_wctg_add_new_item       = get_post_meta($post->ID, 'rc_wctg_add_new_item', true);
        $rc_wctg_edit_item          = get_post_meta($post->ID, 'rc_wctg_edit_item', true);
        $rc_wctg_new_item           = get_post_meta($post->ID, 'rc_wctg_new_item', true);
        $rc_wctg_all_items          = get_post_meta($post->ID, 'rc_wctg_all_items', true);
        $rc_wctg_view_item          = get_post_meta($post->ID, 'rc_wctg_view_item', true);
        $rc_wctg_search_items       = get_post_meta($post->ID, 'rc_wctg_search_items', true);
        $rc_wctg_not_found          = get_post_meta($post->ID, 'rc_wctg_not_found', true);
        $rc_wctg_not_found_in_trash = get_post_meta($post->ID, 'rc_wctg_not_found_in_trash', true);
        $rc_wctg_parent_item_colon  = get_post_meta($post->ID, 'rc_wctg_parent_item_colon', true);
        $rc_wctg_menu_text  		= get_post_meta($post->ID, 'rc_wctg_menu_text', true);
        
        /*
        |--------------------------------------------------------------------------
        | VISIBILITY
        |--------------------------------------------------------------------------
        */
        $rc_wctg_public = get_post_meta($post->ID, 'rc_wctg_public', true);
        if ($rc_wctg_public == "on") {
            $rc_wctg_public = 'true';
        } else {
            $rc_wctg_public = 'false';
        }
        $rc_wctg_show_ui = get_post_meta($post->ID, 'rc_wctg_show_ui', true);
        if ($rc_wctg_show_ui == "on") {
            $rc_wctg_show_ui = 'true';
        } else {
            $rc_wctg_show_ui = 'false';
        }
        $rc_wctg_show_in_menu = get_post_meta($post->ID, 'rc_wctg_show_in_menu', true);
        if ($rc_wctg_show_in_menu == "on") {
            $rc_wctg_show_in_menu = 'true';
        } else {
            $rc_wctg_show_in_menu = 'false';
        }
        $rc_wctg_show_in_admin_bar = get_post_meta($post->ID, 'rc_wctg_show_in_admin_bar', true);
        if ($rc_wctg_show_in_admin_bar == "on") {
            $rc_wctg_show_in_admin_bar = 'true';
        } else {
            $rc_wctg_show_in_admin_bar = 'false';
        }
        $rc_wctg_exclude_from_search = get_post_meta($post->ID, 'rc_wctg_exclude_from_search', true);
        if ($rc_wctg_exclude_from_search == "on") {
            $rc_wctg_exclude_from_search = 'true';
        } else {
            $rc_wctg_exclude_from_search = 'false';
        }
        $rc_wctg_show_in_nav_menus = get_post_meta($post->ID, 'rc_wctg_show_in_nav_menus', true);
        if ($rc_wctg_show_in_nav_menus == "on") {
            $rc_wctg_show_in_nav_menus = 'true';
        } else {
            $rc_wctg_show_in_nav_menus = 'false';
        }
        
        $rc_wctg_show_in_menu_val  	= get_post_meta($post->ID, 'rc_wctg_show_in_menu_val', true);
        $rc_wctg_menu_position  	= get_post_meta($post->ID, 'rc_wctg_menu_position', true);
        $rc_wctg_menu_icon      	= get_post_meta($post->ID, 'rc_wctg_menu_icon', true);
        
        if( $rc_wctg_menu_position == '') { $rc_wctg_menu_position = 100; }
        
        // Show in menu val
        if( isset( $rc_wctg_show_in_menu_val ) && $rc_wctg_show_in_menu_val != '' )
        	$rc_wctg_show_in_menu = $rc_wctg_show_in_menu_val;
        
        /*
        |--------------------------------------------------------------------------
        | OPTIONS
        |--------------------------------------------------------------------------
        */
        $rc_wctg_publicly_queryable = get_post_meta($post->ID, 'rc_wctg_publicly_queryable', true);
        if ($rc_wctg_publicly_queryable == "on") {
            $rc_wctg_publicly_queryable = 'true';
        } else {
            $rc_wctg_publicly_queryable = 'false';
        }
        $rc_wctg_query_var = get_post_meta($post->ID, 'rc_wctg_query_var', true);
        if ($rc_wctg_query_var == "on") {
            $rc_wctg_query_var = 'true';
        } else {
            $rc_wctg_query_var = 'false';
        }
        $rc_wctg_rewrite = get_post_meta($post->ID, 'rc_wctg_rewrite', true);
        if ($rc_wctg_rewrite == "on") {
            $rc_wctg_rewrite = 'true';
        } else {
            $rc_wctg_rewrite = 'false';
        }
        $rc_wctg_has_archive = get_post_meta($post->ID, 'rc_wctg_has_archive', true);
        if ($rc_wctg_has_archive == "on") {
            $rc_wctg_has_archive = 'true';
        } else {
            $rc_wctg_has_archive = 'false';
        }
        $rc_wctg_hierarchical = get_post_meta($post->ID, 'rc_wctg_hierarchical', true);
        if ($rc_wctg_hierarchical == "on") {
            $rc_wctg_hierarchical = 'true';
        } else {
            $rc_wctg_hierarchical = 'false';
        }
        $rc_wctg_can_export = get_post_meta($post->ID, 'rc_wctg_can_export', true);
        if ($rc_wctg_can_export == "on") {
            $rc_wctg_can_export = 'true';
        } else {
            $rc_wctg_can_export = 'false';
        }
        
        /*
        |--------------------------------------------------------------------------
        | Capabilities
        |--------------------------------------------------------------------------
        */
        $rc_wctg_capability_type          		 = get_post_meta($post->ID, 'rc_wctg_capability_type', true);
        $rc_wctg_advanced_capabilities           = get_post_meta($post->ID, 'rc_wctg_advanced_capabilities', true);
        $rc_wctg_capabilities_edit_post          = get_post_meta($post->ID, 'rc_wctg_capabilities_edit_post', true);
        $rc_wctg_capabilities_edit_posts         = get_post_meta($post->ID, 'rc_wctg_capabilities_edit_posts', true);
        $rc_wctg_capabilities_edit_others_posts  = get_post_meta($post->ID, 'rc_wctg_capabilities_edit_others_posts', true);
        $rc_wctg_capabilities_publish_posts      = get_post_meta($post->ID, 'rc_wctg_capabilities_publish_posts', true);
        $rc_wctg_capabilities_read_post          = get_post_meta($post->ID, 'rc_wctg_capabilities_read_post', true);
        $rc_wctg_capabilities_read_private_posts = get_post_meta($post->ID, 'rc_wctg_capabilities_read_private_posts', true);
        $rc_wctg_capabilities_delete_post        = get_post_meta($post->ID, 'rc_wctg_capabilities_delete_post', true);

        if ($rc_wctg_advanced_capabilities == "on") {
        
        	$rc_wctg_capabilities = array(
							            'edit_post'          => $rc_wctg_capabilities_edit_post,
							            'edit_posts'         => $rc_wctg_capabilities_edit_posts,
							            'edit_others_posts'  => $rc_wctg_capabilities_edit_others_posts,
							            'publish_posts'      => $rc_wctg_capabilities_publish_posts,
							            'read_post'          => $rc_wctg_capabilities_read_post,
							            'read_private_posts' => $rc_wctg_capabilities_read_private_posts,
							            'delete_post'        => $rc_wctg_capabilities_delete_post
        							);
        } else {
	        $rc_wctg_capabilities = array();
        }
    
    
        _e('If you need to export this custom post type to another WordPress install, or use still use the custom post type when the "Content Manager" plugin is deactivated, simply copy and paste the code below in the "functions.php" file in your theme folder, and upload the icon too.', 'rc_wctg');
        
		echo "<pre>";
		
        // Create Labels
        echo "
/**
 * Register '$rc_wctg_cpt_name' custom post type
 *
 * Generated by WordPress Content Types Generator
 *
 * @Version     ".RC_WCTG_PLUGIN_VERSION." 
 * @Author      Remi Corson
 * @URL		http://remicorson.com
*/

// Custom Post Type Labels      
&#36labels = array(
	'name'               => _x( '$rc_wctg_plural_name', 'post type general name' ),
	'singular_name'      => _x( '$rc_wctg_singular_name', 'post type singular name' ),
	'add_new'            => _x( '$rc_wctg_add_new', '$rc_wctg_cpt_name' ),
	'add_new_item'       => __( '$rc_wctg_add_new_item' ),
	'edit_item'          => __( '$rc_wctg_edit_item' ),
	'new_item'           => __( '$rc_wctg_new_item' ),
	'all_items'          => __( '$rc_wctg_all_items' ),
	'view_item'          => __( '$rc_wctg_view_item' ),
	'search_items'       => __( '$rc_wctg_search_items' ),
	'not_found'          => __( '$rc_wctg_not_found' ),
	'not_found_in_trash' => __( '$rc_wctg_not_found_in_trash' ),
	'parent_item_colon'  => __( '$rc_wctg_parent_item_colon' ),
	'menu_name'          => __( '$rc_wctg_menu_text' )
);
";
    
if( isset($rc_wctg_advanced_capabilities) && $rc_wctg_advanced_capabilities == 'on') {    
        // Create Capabilities
        echo "
// Custom Post Type Capabilities  
&#36capabilities = array(
	'edit_post'          => '$rc_wctg_capabilities_edit_post',
	'edit_posts'         => '$rc_wctg_capabilities_edit_posts',
	'edit_others_posts'  => '$rc_wctg_capabilities_edit_others_posts',
	'publish_posts'      => '$rc_wctg_capabilities_publish_posts',
	'read_post'          => '$rc_wctg_capabilities_read_post',
	'read_private_posts' => '$rc_wctg_capabilities_read_private_posts',
	'delete_post'        => '$rc_wctg_capabilities_delete_post'
);
";
}
        
        // Create Taxonomies
        $count_taxos = count($rc_wctg_taxos);
        $i = 1;
        echo "
// Custom Post Type Taxonomies  
&#36taxonomies = array(";
    foreach( $rc_wctg_taxos as $rc_wctg_taxo ) {
        echo "'$rc_wctg_taxo'";
        if( $i < $count_taxos)
        	echo ', ';
        $i++;
    }
echo ");
";
        
        // Create Supports
        $count_supports = count($rc_wctg_s);
        $j = 1;
        echo "
// Custom Post Type Supports  
&#36supports = array(";
    foreach( $rc_wctg_s as $rc_wctg_support ) {
        echo "'$rc_wctg_support'";
        if( $j < $count_supports )
        	echo ', ';
        $j++;
    }
echo ");
";
        
        // Create CPT arguments
        $echo_args = "
// Custom Post Type Arguments  
&#36args = array(
    'labels'             => &#36labels,
    'hierarchical'       => $rc_wctg_hierarchical,
    'description'        => '$rc_wctg_description',
    'public'             => $rc_wctg_public,
    'publicly_queryable' => $rc_wctg_publicly_queryable,
    'show_ui'            => $rc_wctg_show_ui,
    'show_in_menu'       => $rc_wctg_show_in_menu,
    'show_in_nav_menus'  => $rc_wctg_show_in_nav_menus,
    'show_in_admin_bar'  => $rc_wctg_show_in_admin_bar,
    'exclude_from_search'=> $rc_wctg_exclude_from_search,
    'query_var'          => $rc_wctg_query_var,
    'rewrite'            => $rc_wctg_rewrite,
    'can_export'         => $rc_wctg_can_export,
    'has_archive'        => $rc_wctg_has_archive,
    'menu_position'      => $rc_wctg_menu_position,
    'taxonomies' 	 => &#36taxonomies,
    'supports'           => &#36supports,";
    
// Advanced capabilities
if( isset($rc_wctg_advanced_capabilities) && $rc_wctg_advanced_capabilities == 'on') {
	$echo_args .= "
	'capabilities' 	 => &#36capabilities,";
}

// Capability Type
if( isset($rc_wctg_capability_type) && $rc_wctg_capability_type != '' )
	$echo_args .= "
    'capability_type'    => '$rc_wctg_capability_type',";
    
// Menu Icon
if( isset($rc_wctg_menu_icon) && $rc_wctg_menu_icon != '' ) {
	$echo_args .= "
    'menu_icon'    	 => '$rc_wctg_menu_icon'";
} else {
	$echo_args .= "
    'menu_icon'    	 => '".RC_WCTG_PLUGIN_URL."assets/images/default_cpt_icon.png'";
}

$echo_args .= "
);

register_post_type( '$rc_wctg_cpt_name', &#36args );";
        
        echo $echo_args;

		echo "</pre>";
    
}

function rc_wctg_cpt_mode_metabox() {
    global $post;
    
    // Mode
    $rc_wctg_mode = get_post_meta($post->ID, 'rc_wctg_mode', true);
    
    ?>
    <p><?php _e('If you are bit lost with this form, you can enable "basic" mode. By enabling this mode you will get rid of advanced fields.', 'rc_wctg'); ?>
    </p>
    <table width="100%" class="rc_wctg_cpt_fields">
    	<tr>
	    	<td><input id="rc_wctg_mode_expert" type="radio" name="rc_wctg_mode" value="expert" <?php checked( $rc_wctg_mode, 'expert' ); ?>> <?php _e('Expert', 'rc_wctg'); ?></td>
	    	<td><input id="rc_wctg_mode_basic" type="radio" name="rc_wctg_mode" value="basic" <?php checked( $rc_wctg_mode, 'basic' ); ?>> <?php _e('Basic', 'rc_wctg'); ?></td>
    	</tr>
    </table>
    <?php
    
}

function rc_wctg_load_cpt_metaboxes() {
    add_meta_box('cpt_metabox', __('Custom Post Type Settings', 'rc_wctg'), 'rc_wctg_cpt_metabox', 'cpt', 'normal');
    add_meta_box('cpt_code_metabox', __('Export', 'rc_wctg'), 'rc_wctg_cpt_export_metabox', 'cpt', 'normal');
    add_meta_box('cpt_mode_metabox', __('Edition Mode', 'rc_wctg'), 'rc_wctg_cpt_mode_metabox', 'cpt', 'side');
}


add_action('save_post', 'rc_wctg_cpt_save_postdata');
add_action('add_meta_boxes', 'rc_wctg_load_cpt_metaboxes');
