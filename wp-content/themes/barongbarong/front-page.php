<?php
/*
// FRONTPAGE template Barong Barong
*/
get_header(); ?>

	<!-- Home header foto -->
	<div class="slider-block">
    	<?php  echo do_shortcode("[layerslider id=\"1\"]"); ?>
    </div>
    
	<div class='container-fluid'>
    	<div class="row homeContent">
      <section class='row-fluid text-center' id='first'>
        
        <?php while ( have_posts() ) : the_post(); ?>
        <div class='col-md-12'>
          <h1><?php the_title(); ?></h1>
        </div>
        <?php the_content() ?>
        
        <?php endwhile; ?>
      </section>
      </div>
      </div>
    </div>
    <div class='container-fluid'>
    	
      <section class='row-fluid text-center' id='second'>
        <div class='col-md-4 content'>
          <h3>
            Visit a retailer
          </h3>
          <img src="<?php bloginfo('template_directory'); ?>/images/maps.jpg" />
          <p class='text-left'>
            Lorem ipsum dolor sit amet, consectetur adipiscing elit. In facilisis, diam ac posuere vestibulum, nunc urna viverra turpis, at tempus urna neque in felis.
          </p>
        </div>
        <div class='col-md-4 content'>
          <h3>
            About the brand
          </h3>
          <p class='text-left'>
            Lorem ipsum dolor sit amet, consectetur adipiscing elit. In facilisis, diam ac posuere vestibulum, nunc urna viverra turpis, at tempus urna neque in felis.
          </p>
          <p class='text-left'>
            Lorem ipsum dolor sit amet, consectetur adipiscing elit. In facilisis, diam ac posuere vestibulum, nunc urna viverra turpis, at tempus urna neque in felis.
          </p>
        </div>
        <div class='col-md-4 content'>
          <h3>
            Visit a retailer
          </h3>
          <div class='icons row'>
            <div class='col-md-3 col-md-offset-3 col-xs-6'>
              <a href='#'>
                <img src="<?php bloginfo('template_directory'); ?>/images/icon-instagram.jpg" />
              </a>
            </div>
            <div class='col-md-3 col-xs-6'>
              <a href='#'>
                <img src="<?php bloginfo('template_directory'); ?>/images/icon-facebook.jpg" />
              </a>
            </div>
          </div>
          <p class='text-left'>
            Lorem ipsum dolor sit amet, consectetur adipiscing elit. In facilisis, diam ac posuere vestibulum, nunc urna viverra turpis, at tempus urna neque in felis.
          </p>
        </div>
      </section>
      

<?php // get_sidebar(); ?>
<?php get_footer(); ?>