$(function(){
	$('a').click(function(e){
		var toggle = $(this).attr('data-toggle');
		if(toggle) {
			e.preventDefault();
		}
		else if (location.pathname.replace(/^\//,'') == this.pathname.replace(/^\//,'') && location.hostname == this.hostname) {
			var target = $(this.hash);
			target = target.length ? target : $('[name=' + this.hash.slice(1) +']');
			if (target.length) {
		        $('html,body').animate({
		          scrollTop: target.offset().top-200
		        }, 500);
		        e.preventDefault();
	      	} 
	    } 
	});

	$('.main-toggle').click(function(){
		$('.main-menu').toggleClass('show');
	});

	$(window).scroll(function(){
        $('.full').each(function(r){
            var pos = $(this).offset().top+200;
            var scrolled = $(window).scrollTop();
            $('.full').css('top', -(scrolled * 0.5) + 'px');         
        });
    });

});

$(window).load(function(){
	var $container = $('#isotope');

	$container.isotope({
	  itemSelector: '.item'
	});
	
	// header
	//$('figure.banner.small').animate({'height':'50%', 'opacity':'100'}, 1500);;
	
});

jQuery(function ($)
{
	$(".product-attributes a").click(function (e)
	{
		e.preventDefault();
		if (IS_SINGLE_PRODUCT_AJAX_WORKING) return;
		IS_SINGLE_PRODUCT_AJAX_WORKING = true;
		var $this = $(this);
		var $root = $this.parents(".product-attributes").first();
		$root.find("a").removeClass("active");
		$this.addClass("active");
		var productID = $(".buy-form input[name=add-to-cart]").val();
		
		var variations = {};
		$(".product-attributes a.active").each(function ()
		{
			var $this = $(this);
			variations[$this.data("key")] = $this.data("val");
		});
		$.post(
			window.ajaxURL,
			{ product_id: productID, "variations[]": variations, action: "get_variation_id" },
			function (resp)
			{
				if (resp != null)
				{
					try
					{
						resp = jQuery.parseJSON(resp);
					} catch (err) {}
					if (typeof resp === "object")
					{
						$(".buy-form input[name=variation_id]").val(resp.var_id);
						$(".buy-form .attrs *").remove();
						$(".price").html(resp.price);
						IS_PRODUCT_IN_STOCK = resp.in_stock;
						$.each(variations, function (key, value)
						{
							$(".buy-form .attrs").append("<input type='hidden' name='" + key + "' value='" + value + "'>");
						});
					}
				}				
			}
		).always(function ()
		{
			IS_SINGLE_PRODUCT_AJAX_WORKING = false;
		});
	}).filter(".active").last().trigger("click");
	
	$("input[type=submit].add-to-bag").click(function (e)
	{
		if (IS_SINGLE_PRODUCT_AJAX_WORKING) e.preventDefault();
		if (!IS_PRODUCT_IN_STOCK) e.preventDefault();
	});
	
	var elevateZoom = function ()
	{
		$(".zoomContainer").remove();
		$(".product-large img").elevateZoom({ responsive: true, imageCrossfade: true, zoomWindowPosition: 3, zoomWindowOffety: -15, borderSize: 2, borderColour: "#000" });
	}
	elevateZoom();
	
	$(document).on("click", ".products-small a", function (e)
	{
		e.preventDefault();
		var $largeImg = $(".product-large img");
		var $currImg = $(this).find("img");
		
		var thumb = $largeImg.data("thumb");
		var med = $largeImg.attr("src");
		var large = $largeImg.data("zoom-image");	
		
		$largeImg.fadeOut("fast", function ()
		{
			$(".product-large *").remove();
			var img = $("<img src='" + $currImg.data("med") + "' data-zoom-image='" + $currImg.data("large") + "' data-thumb='" + $currImg.attr("src") + "' style='display: none;' />");
			$(".product-large").append(img);
			$(".product-large img").fadeIn("fast");
		});
		
		$currImg.fadeOut("fast", function ()
		{
			$currImg.attr("src", thumb);
			$currImg.data("med", med);
			$currImg.data("large", large);
			$currImg.fadeIn("fast", elevateZoom);
		});
	});
});

