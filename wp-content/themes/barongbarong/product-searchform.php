  <form class='search-form' role="search" method="get" id="searchform" action="<?php esc_url( home_url( '/'  ) ) ?>">
	<input class='search' name='s' placeholder='Search ...' type='text' value="<?php _e(esc_attr(@$_GET['s'])) ?>">
	<input class='search-submit' type='submit' value=''>
	<input type="hidden" name="post_type" value="product">
  </form>
  <script type="text/javascript">
	jQuery(function ($)
	{
		$(".search-form input[type=text].search").keyup(function (e)
		{
			var key = e.keyCode || e.which;
			if (key === 13) $(".search-form").submit();
		});
	});
  </script>