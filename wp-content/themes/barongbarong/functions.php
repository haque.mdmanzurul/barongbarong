<?php
	if ( function_exists('register_sidebar') ) 
		register_sidebar(array(
			'name'=> 'WooCommerce Archive Sidebar',
			'id' => 'woocommerce_archive_sidebar',
			'before_widget' => '<li id="%1$s" class="widget %2$s">',
			'after_widget' => '</li>',
			'before_title' => '',
			'after_title' => '',
		));
	
	class WC_Meta_Box_Product_Details_Care {

		public static function output( $post ) {
			$settings = array(
				'textarea_name'	=> 'details_and_care',
				'quicktags' 	=> array( 'buttons' => 'em,strong,link' ),
				'tinymce' 	=> array(
					'theme_advanced_buttons1' => 'bold,italic,strikethrough,separator,bullist,numlist,separator,blockquote,separator,justifyleft,justifycenter,justifyright,separator,link,unlink,separator,undo,redo,separator',
					'theme_advanced_buttons2' => '',
				),
				'editor_css'	=> '<style>#wp-excerpt-editor-container .wp-editor-area{height:175px; width:100%;}</style>'
			);

			$content = get_post_meta( $post->ID, '_details_and_care', true );
			$content = stripslashes( $content );
			wp_editor( $content, 'detailsandcare', $settings );
		}

	}
	
	class WC_Meta_Box_Product_Delivery_Returns {

		public static function output( $post ) {
			$settings = array(
				'textarea_name'	=> 'delivery_and_returns',
				'quicktags' 	=> array( 'buttons' => 'em,strong,link' ),
				'tinymce' 	=> array(
					'theme_advanced_buttons1' => 'bold,italic,strikethrough,separator,bullist,numlist,separator,blockquote,separator,justifyleft,justifycenter,justifyright,separator,link,unlink,separator,undo,redo,separator',
					'theme_advanced_buttons2' => '',
				),
				'editor_css'	=> '<style>#wp-excerpt-editor-container .wp-editor-area{height:175px; width:100%;}</style>'
			);

			$content = get_post_meta( $post->ID, '_delivery_and_returns', true );
			$content = stripslashes( $content );			
			wp_editor( $content, 'deliveryandreturns', $settings );
		}

	}
		
	function single_product_woocommerce_custom_meta() {
		add_meta_box( 'details_and_care', __( 'Product Details & Care', 'woocommerce' ), 'WC_Meta_Box_Product_Details_Care::output', 'product', 'normal' );
		add_meta_box( 'delivery_and_returns', __( 'Product Delivery & Returns', 'woocommerce' ), 'WC_Meta_Box_Product_Delivery_Returns::output', 'product', 'normal' );
	}
	add_action( 'add_meta_boxes', 'single_product_woocommerce_custom_meta' );
	
	function template_page_save_metabox_fields($post_id) {
		$post_type = get_post_type($post_id);
		
		if (defined('DOING_AUTOSAVE') && DOING_AUTOSAVE) 
			return;
	
		if ($_POST['post_type'] == 'product') {
			if (!current_user_can('edit_page', $post_id))
				return;
		}
		
		$details_and_care = isset($_POST['details_and_care']) ? $_POST['details_and_care'] : '';
		$delivery_and_returns = isset($_POST['delivery_and_returns']) ? $_POST['delivery_and_returns'] : '';

		update_post_meta($post_id, '_details_and_care', $details_and_care);
		update_post_meta($post_id, '_delivery_and_returns', $delivery_and_returns);
	}
	
	add_action('save_post', 'template_page_save_metabox_fields');	
	
	function get_cart_page_url() {
		global $woocommerce;
		return $woocommerce->cart->get_cart_url();	
		$args = array(
			'posts_per_page' => 1,
			'post_type' => 'page',
			'post_status' => 'publish',
			'meta_query' => array(
				array(
					'key' => '_wp_page_template',
					'value' => 'page-cart.php',
				)
			)
		);
		$my_query = new WP_Query($args);
		if (count($my_query->posts) == 1) return get_permalink($my_query->posts[0]->ID);
		return null;
	}
	
	function out_of_stock_html($echo = false) {
		$html = '<span style="color: red;">Out of stock</span>';
		if ($echo)
			_e($html);
		else
			return $html;
	}
	
	add_action('wp_ajax_nopriv_get_variation_id', 'ajax_get_variation_id');
	add_action('wp_ajax_get_variation_id', 'ajax_get_variation_id');	
	
	function ajax_get_variation_id() {
		if (isset($_POST['product_id']) && !empty($_POST['product_id'])) {
			$product_id = $_POST['product_id'];
			$variations = $_POST['variations'];
			foreach ($variations as $v_k => $v_a) $variations[$v_k] = str_replace(' ', '-', $v_a);
			$variation_id = get_variation_id($product_id, $variations);
			if (!empty($variation_id) && is_numeric($variation_id) && $variation_id > 0) {
				$variation = new WC_Product_Variation($variation_id);
				_e(
					json_encode(
						array(
							'var_id'   => $variation_id,
							'price'    => $variation->is_in_stock() ? str_replace('"amount"', "'amount'", $variation->get_price_html()) : out_of_stock_html(),
							'in_stock' => $variation->is_in_stock()
						)
					)
				);
				exit;
			}
		}
		_e('0');
		exit;
	}	
	
	function get_variation_id($product_id, $variations) {
		$product = get_product($product_id);
		$vars = $product->get_available_variations();
		$fallback_var_id = 0;
		$exact_var_id = 0;
		$variation_count = count($variations);
		foreach ($vars as $v) {
			$empty_count = 0;
			$exact_count = 0;		
			foreach ($v['attributes'] as $attr_key => $attr_val) {
				$i = 0;
				$key_found = -1;
				foreach ($variations as $variation) {
					if (isset($variation[$attr_key])) {
						$key_found = $i;
						break;
					}
					$i++;
				}
				if ($key_found > -1) {
					if (isset($variations[$key_found][$attr_key])) {
						if ($variations[$key_found][$attr_key] == $attr_val || empty($attr_val)) {
							$exact_count++;
							if ($exact_count == $variation_count) {
								$exact_var_id = $v['variation_id'];
							}
						} 
					}
				}
			}
		}
		
		if ($exact_var_id > 0) return $exact_var_id;
		if ($fallback_var_id > 0) return $fallback_var_id;
	}
	
	add_image_size('552x426', 552, 426, false);
	add_image_size('219x208', 219, 208, false);
	add_image_size('84x80', 84, 80, false);
	
	function theme_scripts() {
		wp_register_script( 'wc-price-slider', WC()->plugin_url() . '/assets/js/frontend/price-slider' . $suffix . '.js', array( 'jquery-ui-slider' ), WC_VERSION, true );

		wp_localize_script( 'wc-price-slider', 'woocommerce_price_slider_params', array(
			'currency_symbol' 	=> get_woocommerce_currency_symbol(),
			'currency_pos'      => get_option( 'woocommerce_currency_pos' ),
			'min_price'			=> isset( $_GET['min_price'] ) ? esc_attr( $_GET['min_price'] ) : '',
			'max_price'			=> isset( $_GET['max_price'] ) ? esc_attr( $_GET['max_price'] ) : ''
		) );
		
		wp_enqueue_script( 'wc-price-slider' );
	}

	add_action( 'wp_enqueue_scripts', 'theme_scripts' );
	
    function woocommerce_breadcrumb( $args = array() ) {
        $defaults = array(
            'delimiter'  => ' &rsaquo; ',
            'wrap_before'  => '<div id="breadcrumb" itemprop="breadcrumb">',
            'wrap_after' => '</div>',
            'before'   => '',
            'after'   => '',
            'home'    => null
        );

        $args = wp_parse_args( $args, $defaults  );

        woocommerce_get_template( 'shop/breadcrumb.php', $args );
    }
	
	function get_previous_page_url($fallback) {
		$link = '';
		$ref = $_SERVER['HTTP_REFERER'];
		if( $ref != "" ){
			$refDomain = parse_url($ref);
			$refDomain = $refDomain['host'];
			if($refDomain == $_SERVER['HTTP_HOST']){
				$link = $ref;
			} else {
				$link = $fallback;
			}
		} else {
			$link = $fallback;
		}
		return $link;
	}
	
	class TitleSeparatorWidget extends WP_Widget {

		public function __construct() {
			parent::__construct(
				'title_separator_widget',
				'Title Separator Widget',
				array('description' => 'Title Separator')
			);
		}

		public function widget($args, $instance) {
			extract( $args );	
			$title = apply_filters( 'widget_title', empty( $instance['title'] ) ? '' : $instance['title'] );
			echo $before_widget;
			if ( !empty( $title ) ) { echo '</ul><h4>' . $title . '</h4><ul>'; }		
			echo $after_widget;
		}

		public function form($instance) {
			// TITLE
			if ( isset( $instance[ 'title' ] ) ) {
				$title = $instance[ 'title' ];
			} else {
				$title = '';
			}
			?>
				<p>
					<label for="<?php echo $this->get_field_name( 'title' ); ?>"><?php _e( 'Title:' ); ?></label> 
					<input class="widefat" id="<?php echo $this->get_field_id( 'title' ); ?>" name="<?php echo $this->get_field_name( 'title' ); ?>" type="text" value="<?php echo esc_attr( $title ); ?>" />
				</p>
			<?php 
		}

		public function update($new_instance, $old_instance) {
			$instance = array();
			$instance['title'] = ( ! empty( $new_instance['title'] ) ) ? strip_tags( $new_instance['title'] ) : '';
			return $instance;
		}
	}
	
	function theme_widgets() {
		register_widget('TitleSeparatorWidget');
	}
	
	add_action('widgets_init', 'theme_widgets');
	
	
add_action("wp_ajax_get_retailer_country_cities", "get_retailer_country_cities");
add_action("wp_ajax_nopriv_get_retailer_country_cities", "get_retailer_country_cities");
function get_retailer_country_cities()
{
    global $wpdb;

	$query_available_country = $wpdb->get_results("SELECT * FROM ".$wpdb->prefix."postmeta WHERE meta_key='country'");
	//print_r($query_available_country);
	$retailers_country = array();
	$retailers_city = array();
	if(count($query_available_country)>0){
		foreach($query_available_country as $c){
			$data =  maybe_unserialize($c->meta_value);
			if(!in_array($data['country_name'],$retailers_country))
			array_push($retailers_country,$data['country_name']);
			if(!in_array($data['city'],$retailers_city))
			array_push($retailers_city,$data['city_name']);							
		}
	}

    $country_id = (int) trim($_REQUEST['countryId']);
    $country_name = trim($_REQUEST['countryName']);

    $cities_db        = $wpdb->get_results("SELECT * FROM ".$wpdb->prefix."cities WHERE  country='".$country_id."' ORDER BY city ASC");
    $cities               = array();

    if ($cities_db)
    {
        foreach ($cities_db AS $city)
        {
			if(!in_array($city->city,$retailers_city)) continue;
            $cities[$city->id] = $city->city;
        }
    }

   $type = 'retailers';
   $args=array(
	  'post_type' => $type,
	  'post_status' => 'publish',
	  'posts_per_page' => -1,
	  'caller_get_posts'=> 1
	  );
	$my_query = null;
	$my_query = new WP_Query($args);
	$retailers = "";
	$locationData = array();
		if( $my_query->have_posts() ) {
		  while ($my_query->have_posts()) : $my_query->the_post();
				$country = get_field('country');			

				if(!in_array($country_name, $country))
					continue;


				$retailers .="<div class='col-md-4 store'>"; 

				  $image = get_field('logo');

				  if( !empty($image) ): 

					//$retailers .='<img src="'.$image['url'].'" alt="'.$image['alt'].'" />';

				  endif;

			  $retailers .="
			  <div class='col-md-10'>
				<h4>".get_the_title()."
				</h4>
				<p>".get_field('address')."</p>
				<p>phone: ".get_field('phone')."<br>email: ".get_field('email')."<br>web: ".get_field('website')."</p>
			  </div>
			</div>";
			
			$address = get_field('address').', '.$country['city_name'].' '.$country['country_name'];
	
            //$address = $postdata['street_address']." ".$postdata['city']." ".$postdata['zip'];
            $urlEncodedAddress = urlencode($address);
            $geocodedAddress = file_get_contents("http://maps.googleapis.com/maps/api/geocode/json?address=$urlEncodedAddress&sensor=false");
            $decodedAddress = json_decode($geocodedAddress);
			$formattedAddress = '';
            if ($decodedAddress && ($decodedAddress->status == 'OK')){
                $formattedAddress = $decodedAddress->results[0]->formatted_address;
                $latitude = $decodedAddress->results[0]->geometry->location->lat;
                $longitude = $decodedAddress->results[0]->geometry->location->lng;
            } else {
                $formattedAddress = $address;
                $latitude = "";
                $longitude = "";
            }

            array_push($locationData, array(
			    'title' => get_the_title(),
                'addressline' => $formattedAddress,
                'lat' => $latitude,
                'lon' => $longitude,
                'status' => 'active'
            ));			
		  endwhile;
	  }
   		 echo json_encode(array("data"=>$cities,"retailers"=>$retailers,"locationdata"=>$locationData,"type"=>"success"));

    die();
}	

add_action("wp_ajax_get_city_retailers", "get_city_retailers");
add_action("wp_ajax_nopriv_get_city_retailers", "get_city_retailers");
function get_city_retailers()
{
    global $wpdb;

    $country_id = (int) trim($_REQUEST['countryId']);
    $country_name = trim($_REQUEST['countryName']);
	$cityName = trim($_REQUEST['cityName']);

   $type = 'retailers';
   $args=array(
	  'post_type' => $type,
	  'post_status' => 'publish',
	  'posts_per_page' => -1,
	  'caller_get_posts'=> 1
	  );
	$my_query = null;
	$my_query = new WP_Query($args);
	$retailers = "";
	$locationData = array();
		if( $my_query->have_posts() ) {
		  while ($my_query->have_posts()) : $my_query->the_post();
				$country = get_field('country');			

				if(!in_array($country_name, $country))
					continue;

				if(!in_array($cityName, $country))
					continue;
					
				$retailers .="<div class='col-md-4 store'>"; 

				  $image = get_field('logo');

				  if( !empty($image) ): 

					$retailers .='<img src="'.$image['url'].'" alt="'.$image['alt'].'" />';

				  endif;

			  $retailers .="<div class='col-md-10'>
				<h4>".get_the_title()."
				</h4>
				<p>".get_field('address')."</p>
				<p>phone: ".get_field('phone')."<br>email: ".get_field('email')."<br>web: ".get_field('website')."</p>
			  </div>
			</div>";
			
			$address = get_field('address').', '.$country['city_name'].' '.$country['country_name'];
	
            //$address = $postdata['street_address']." ".$postdata['city']." ".$postdata['zip'];
            $urlEncodedAddress = urlencode($address);
            $geocodedAddress = file_get_contents("http://maps.googleapis.com/maps/api/geocode/json?address=$urlEncodedAddress&sensor=false");
            $decodedAddress = json_decode($geocodedAddress);
			$formattedAddress = '';
            if ($decodedAddress && ($decodedAddress->status == 'OK')){
                $formattedAddress = $decodedAddress->results[0]->formatted_address;
                $latitude = $decodedAddress->results[0]->geometry->location->lat;
                $longitude = $decodedAddress->results[0]->geometry->location->lng;
            } else {
                $formattedAddress = $address;
                $latitude = "";
                $longitude = "";
            }

            array_push($locationData, array(
			    'title' => get_the_title(),
                'addressline' => $formattedAddress,
                'lat' => $latitude,
                'lon' => $longitude,
                'status' => 'active'
            ));			
		  endwhile;
	  }
   		 echo json_encode(array("data"=>$cities,"retailers"=>$retailers,"locationdata"=>$locationData,"type"=>"success"));

    die();
}
// get lat long for retailers

function getRetailerGoogleMapPin(){
	   $locationData = array();
       $type = 'retailers';
            $args=array(
              'post_type' => $type,
              'post_status' => 'publish',
              'posts_per_page' => -1,
              'caller_get_posts'=> 1
              );
            $my_query = null;
            $my_query = new WP_Query($args);
            $retailers = "";
            if( $my_query->have_posts() ) {
              while ($my_query->have_posts()) : $my_query->the_post();
              		$country = get_field('country');
					
			$address = get_field('address').', '.$country['city_name'].' '.$country['country_name'];
	
            //$address = $postdata['street_address']." ".$postdata['city']." ".$postdata['zip'];
            $urlEncodedAddress = urlencode($address);
            $geocodedAddress = file_get_contents("http://maps.googleapis.com/maps/api/geocode/json?address=$urlEncodedAddress&sensor=false");
            $decodedAddress = json_decode($geocodedAddress);
			$formattedAddress = '';
            if ($decodedAddress && ($decodedAddress->status == 'OK')){
                $formattedAddress = $decodedAddress->results[0]->formatted_address;
                $latitude = $decodedAddress->results[0]->geometry->location->lat;
                $longitude = $decodedAddress->results[0]->geometry->location->lng;
            } else {
                $formattedAddress = $address;
                $latitude = "";
                $longitude = "";
            }

            array_push($locationData, array(
			    'title' => get_the_title(),
                'addressline' => $formattedAddress,
                'lat' => $latitude,
                'lon' => $longitude,
                'status' => 'active'
            ));
/*              		if(!in_array($country_name, $country))
              			continue;


					$retailers .="<div class='col-md-4 store'>
                  <div class='col-md-2'>"; 

                      $image = get_field('logo');

                      if( !empty($image) ): 

                        $retailers .='<img src="'.$image['url'].'" alt="'.$image['alt'].'" />';

                      endif;

                  $retailers .="</div>
                  <div class='col-md-10'>
                    <h4>".get_the_title()."
                    </h4>
                    <p>".get_field('address')."</p>
                    <p>phone: ".get_field('phone')."<br>email: ".get_field('email')."<br>web: ".get_field('website')."</p>
                  </div>
                </div>";*/
              endwhile;
          }	
		  
		  return json_encode($locationData);


}
?>