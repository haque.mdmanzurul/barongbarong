<?php
/**
 * The Header template for our theme
 *
 * Displays all of the <head> section and everything up till <div id="main">
 *
 * @package WordPress
 * @subpackage Twenty_Twelve
 * @since Twenty Twelve 1.0
 */
?><!DOCTYPE html>
<!--[if IE 7]>
<html class="ie ie7" <?php language_attributes(); ?>>
<![endif]-->

<!--[if IE 8]>
<html class="ie ie8" <?php language_attributes(); ?>>
<![endif]-->
<!--[if !(IE 7) | !(IE 8)  ]><!-->
<html <?php language_attributes(); ?>>
  <head>
    <title><?php wp_title( '|', true, 'right' ); ?></title>
    <meta charset='utf-8'>
    <meta content='Description' name='Barong Barong'>
    <meta content='1 day' name='revisit-after'>
    <meta content='width=device-width, initial-scale=1' name='viewport'>
    
    <link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>" />
    <link href='http://netdna.bootstrapcdn.com/bootstrap/3.1.1/css/bootstrap.min.css' rel='stylesheet'>
    <link href='http://fonts.googleapis.com/css?family=Open+Sans:400italic,600italic,700italic,400,600,700' rel='stylesheet'>
    <link href='http://fonts.googleapis.com/css?family=Vollkorn:400italic' rel='stylesheet'>
    
    
    <!--[if lt IE 9]>
    <script src="<?php bloginfo('template_directory'); ?>/javascripts/html5shiv.js" type="text/javascript"></script>
    <script src="<?php bloginfo('template_directory'); ?>/javascripts/respond.min.js" type="text/javascript"></script>
    <![endif]-->
    
    <?php wp_head(); ?>
    
    <link href='<?php bloginfo('template_directory'); ?>/style.css' media='screen' rel='stylesheet'>
    
    <script type="text/javascript">
		window.ajaxURL = "<?php _e(admin_url('admin-ajax.php')) ?>";
		$(document).ready(function(e) {
			$('.widget_price_filter').click(function(){
				$('.widget_price_filter > form').slideToggle();
			});       
			
			$('#woocommerce_layered_nav-2').click(function(){
				$('#woocommerce_layered_nav-2 > ul').slideToggle();
			});	
			
			$('#woocommerce_layered_nav-3').click(function(){
				$('#woocommerce_layered_nav-3 > ul').slideToggle();
			});					     
        });

	</script>
  </head>

<body <?php body_class(); ?>>


<!-- begin header -->

<header class='container-fluid headerMenu'>
      <div class='nav-toggle main-toggle'></div>
      <div class='col-md-12'>
        <nav id='top'>
          <ul>
            <!--
            <li>
              <a href='#'>Login</a>
            </li>
            -->
            <li>
              <div class='open-cart' href='#'>
                <img src="<?php bloginfo('template_directory'); ?>/images/basket.jpg" />
                <?php _e(WC()->cart->cart_contents_count) ?>
                <div class='cart-info'>
                  <div class='title'>
                    Your Shopping bag (<?php _e(WC()->cart->cart_contents_count) ?>)
                  </div>
			<?php
			foreach ( WC()->cart->get_cart() as $cart_item_key => $cart_item ) {
				$_product     = apply_filters( 'woocommerce_cart_item_product', $cart_item['data'], $cart_item, $cart_item_key );
				$product_id   = apply_filters( 'woocommerce_cart_item_product_id', $cart_item['product_id'], $cart_item, $cart_item_key );

				if ( $_product && $_product->exists() && $cart_item['quantity'] > 0 && apply_filters( 'woocommerce_cart_item_visible', true, $cart_item, $cart_item_key ) ) {
			?>
                  <div class='item row'>
                    <div class='col-md-4'>
						<?php
							$thumbnail = wp_get_attachment_image_src(get_post_thumbnail_id($product_id), '84x80');
							if (!empty($thumbnail)) echo '<img src="' . $thumbnail[0] . '" />';
						?>	
                    </div>
                    <div class='col-md-6'>
                      <div class='item-title'>
                        <?php _e($_product->get_title()) ?>
                      </div>
                      <div class='info'>
                        color: <?php 
						$term = get_term_by('slug', @$cart_item['variation']['attribute_pa_maat'], 'pa_maat');
						if ($term) _e($term->name);						
						?>
                      </div>
                      <div class='info'>
                        material: <?php 
							$term = get_term_by('slug', @$cart_item['variation']['attribute_pa_material'], 'pa_material');
							if ($term) _e($term->name);		
						?>
                      </div>
                      <div class='info'>
                        amount: <?php _e($cart_item['quantity']) ?>
                      </div>
                      <div class='info'>
                        price: <?php echo apply_filters( 'woocommerce_cart_item_subtotal', WC()->cart->get_product_subtotal( $_product, $cart_item['quantity'] ), $cart_item, $cart_item_key ); ?>
                      </div>
                    </div>
                    <div class='col-md-2'>
					  <?php echo apply_filters( 'woocommerce_cart_item_remove_link', sprintf( '<a href="%s" class="remove" title="%s"> <img src="' . get_bloginfo('template_directory') . '/images/remove.png" /></a>', esc_url( WC()->cart->get_remove_url( $cart_item_key ) ), __( 'Remove this item', 'woocommerce' ) ), $cart_item_key ); ?>
                    </div>
                  </div>
              <?php } ?>
			<?php } ?>
                  <div class='total row'>
                    <div class='col-md-6 col-md-offset-4'>
                      <div class='info'>
                        total: &euro; <?php _e(WC()->cart->cart_contents_total) ?>
                      </div>
                    </div>
                  </div>
                  <a class='btn btn-lg' href='<?php _e(get_cart_page_url()) ?>'>Checkout</a>
                </div>
              </div>
            </li>
          </ul>
        </nav>
      </div>
      <div class='col-md-5'>
        <nav class='main-menu first'>
          <ul>
            <li>
              <a href='/'>Home</a>
            </li>
            <li>
              <a href='<?php bloginfo('url'); ?>/about/'>About</a>
            </li>
            <li>
              <a href='<?php bloginfo('url'); ?>/shop/'>Shop</a>
            </li>
          </ul>
        </nav>
      </div>
      <div class='col-md-2 text-center'>
        <a href='/'>
          <img class="logo" src="<?php bloginfo('template_directory'); ?>/images/logo.png" title="<?php bloginfo( 'name' ); ?>" />
        </a>
      </div>
      <div class='col-md-5'>
        <nav class='main-menu second'>
          <ul>
            <li>
              <a href='<?php bloginfo('url'); ?>/gallery/'>Inspiration</a>
            </li>
            <li>
              <a href='<?php bloginfo('url'); ?>/retailers/'>Retailers</a>
            </li>
            <li>
              <a href='<?php bloginfo('url'); ?>/contact/'>Contact</a>
            </li>
          </ul>
        </nav>
      </div>
    </header>

<!-- eind header -->