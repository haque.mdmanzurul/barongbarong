<?php
/**
 * The Template for displaying all single posts
 *
 * 
 * 
 *
 */

get_header(); ?>

	<div class='container-fluid'>
    
        <section class='row-fluid text-left' id='first' style="margin-top:200px;">
        
            <?php while ( have_posts() ) : the_post(); ?>
            
            <div class='col-md-12 text-center'>
            
                <h1><?php the_title(); ?> </h1>
                
            </div>
            
           <?php the_content() ?>
        
        </section>
        
        <?php endwhile; // end of the loop. ?>

<?php get_footer(); ?>