<?php
/**
 * The main template file
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * For example, it puts together the home page when no home.php file exists.
 *
 * @link http://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage Twenty_Twelve
 * @since Twenty Twelve 1.0
 */

get_header(); ?>

	<div class='container-fluid'>
    
        <section class='row-fluid text-left' id='first'>
        
            <?php while ( have_posts() ) : the_post(); ?>
            
            <div class='col-md-12 text-center'>
            
                <h1><?php the_title(); ?> </h1>
                
            </div>
            
            <?php get_template_part( 'content', 'page' ); ?>
        
        </section>
	<?php endwhile; // end of the loop. ?>
<?php get_footer(); ?>