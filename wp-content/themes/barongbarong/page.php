<?php
/**
 * The template for displaying all pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site will use a
 * different template.
 *
 * @package WordPress
 * @subpackage Twenty_Twelve
 * @since Twenty Twelve 1.0
 */

get_header(); ?>

	<!-- header foto -->
    <figure class='banner small'>
        <img class="full" src="<?php bloginfo('template_directory'); ?>/images/header-page.jpg" />
    </figure>
    
    <div class='container-fluid'>
    
        <section class='row-fluid text-left' id='first'>
        
        <div class='col-md-12 crumbtrail'>
	  <a href="<?php _e(get_bloginfo('url')) ?>">home</a>&nbsp;/&nbsp;<a href="<?php get_permalink(); ?>"><?php the_title(); ?></a>
	  
	</div>
        
            <?php while ( have_posts() ) : the_post(); ?>
            
            <div class='col-md-12 text-center'>
            
                <h1><?php the_title(); ?> </h1>
                
            </div>
            
            <?php // get_template_part( 'content', 'page' ); ?>
			<?php the_content() ?>
        
        </section>

<?php endwhile; // end of the loop. ?>

<?php // get_sidebar(); ?>
<?php get_footer(); ?>